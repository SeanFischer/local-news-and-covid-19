# load libraries
library(dplyr)
library(lfe)
library(CBPS)
library(ggplot2)
library(patchwork)
library(lubridate)

# load mobility data with covariates
load("data/mobility_reports_4_30.RData")

# filter out buggy cases
google_mobility_2 <-
  google_mobility_2 %>%
  distinct() %>%
  group_by(fips) %>%
  filter(n() <= 76) %>%
  mutate(days_in_period = case_when(date <= mdy("3-11-2020") ~ 
                                      date - mdy("2-14-2020"),
                                    after_order == 0 & date > mdy("3-11-2020") ~
                                      date - mdy("3-11-2020"),
                                    after_order == 1 ~
                                      date - order_date))

# function to generate predictions from clustered errors
# https://stackoverflow.com/questions/3790116/using-clustered-covariance-matrix-in-predict-lm
predict.rob <- function(x, clcov, newdata){
  if(missing(newdata)){ newdata <- x$model }
  tt <- terms(x)
  Terms <- delete.response(tt)
  m.mat <- model.matrix(Terms, data = newdata)
  m.coef <- x$coef
  fit <- as.vector(m.mat %*% x$coef)
  se.fit <- sqrt(diag(m.mat%*%clcov%*%t(m.mat)))
  return(list(fit=fit,se.fit=se.fit))
}

# first DiD model with covariate adjustment
m_residential <- lm(residential_std ~ 
                      news_desert_rev * time_period +
                      cases_std_all +
                      deaths_std_all + 
                      total_population_std + # county pop.
                      POP_SQMI_std + # county pop. density
                      median_age_std  + # county median age
                      pct_non_white_std  + # share of pop not white
                      pct_college_deg_std + # share of pop with any college deg
                      pct_below_pov_lvl_std +  # share of pop below pov line
                      pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
                      pct_with_comp_broadband_std + # share with computer and broadband int
                      pct_speak_only_english_std + # share that only speak english at home
                      PCT_LACCESS_POP15_std + # share with low access to food
                      state_capital + # state capital?
                      dem_pct_std,
                    data = google_mobility_2)
# get clustered standard errors
vcov_state_county <- multiwayvcov::cluster.vcov(m_residential,
                                                ~ state_abrv + fips)
# summary output using clustered standard errors
lmtest::coeftest(m_residential, vcov = vcov_state_county)
confint(lmtest::coeftest(m_residential, vcov = vcov_state_county))
# generate new data for predictions
new_data_res <- expand.grid(
  news_desert_rev = c(0, 1),
  time_period = c("Time 1", "Time 2", "Time 3"),
  cases_std_all = 0,
  deaths_std_all = 0,
  total_population_std = 0,
  POP_SQMI_std = 0,
  median_age_std = 0,
  pct_non_white_std = 0,
  pct_college_deg_std = 0,
  pct_below_pov_lvl_std = 0,
  pct_above_2_pov_lvl_std = 0,
  pct_with_comp_broadband_std = 0,
  pct_speak_only_english_std = 0,
  PCT_LACCESS_POP15_std = 0,
  state_capital = 0,
  dem_pct_std = 0)

# predict outcomes and get estimates
new_data_res$outcome <- predict.rob(m_residential, 
                                    vcov_state_county, 
                                    new_data_res)$fit
# predict outcomes and get se for estimates
new_data_res$se_outcome <- predict.rob(m_residential, 
                                       vcov_state_county, 
                                       new_data_res)$se.fit
# continuous scale time period labels
new_data_res$time_period_2 <- ifelse(new_data_res$time_period == "Time 1", "Before March 12", 
                                     ifelse(new_data_res$time_period == "Time 2", 
                                            "March 12 - Statewide Order", "After Statewide Order"))
new_data_res$time_period_2 <- factor(new_data_res$time_period_2,
                                     levels = c("Before March 12",
                                                "March 12 - Statewide Order",
                                                "After Statewide Order"))

# plot predictions
ggplot(new_data_res %>% 
         mutate(
           lwr = outcome - 1.96 * se_outcome,
           upr = outcome + 1.96 * se_outcome), 
       aes(x = news_desert_rev, y = outcome)) + 
  geom_hline(yintercept = 0, lty = 2) +
  # geom_point() +
  geom_line() +
  geom_errorbar(aes(ymin = lwr, ymax = upr), widht = 0.5) +
  facet_wrap(~ time_period_2) +
  xlab("News Desert Status") + 
  ylab("Predicted Standard-Deviation Change in Mobility") +
  ggtitle("A. Staying Home - Unweighted") +
  scale_x_continuous(breaks = c(0, 1),
                     labels = c("Yes", "No")) +
  # scale_color_brewer(name = "Time Period", palette = "Paired") +
  guides(fill = FALSE) +
  fischeR::theme_saf_no_font() + 
  theme(panel.border = element_blank(), # remove borders
        # add x-axis line
        axis.line.x = element_line(),
        axis.line.y = element_line(),
        strip.text = element_text(size = 9,
                                  colour = "black",
                                  family = "Helvetica Neue"),
        strip.background = element_rect(colour = "white",
                                        fill = "white")) -> p1

m_recreation <- lm(recreation_std ~ 
                     news_desert_rev * time_period +
                     cases_std_all +
                     deaths_std_all + 
                     total_population_std + # county pop.
                     POP_SQMI_std + # county pop. density
                     median_age_std  + # county median age
                     pct_non_white_std  + # share of pop not white
                     pct_college_deg_std + # share of pop with any college deg
                     pct_below_pov_lvl_std +  # share of pop below pov line
                     pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
                     pct_with_comp_broadband_std + # share with computer and broadband int
                     pct_speak_only_english_std + # share that only speak english at home
                     PCT_LACCESS_POP15_std + # share with low access to food
                     state_capital + # state capital?
                     dem_pct_std,
                   data = google_mobility_2)
vcov_state_county_rec <- multiwayvcov::cluster.vcov(m_recreation,
                                                    ~ state_abrv + fips)
lmtest::coeftest(m_recreation, vcov = vcov_state_county_rec)
confint(lmtest::coeftest(m_recreation, vcov = vcov_state_county_rec))

new_data_rec <- expand.grid(
  news_desert_rev = c(0, 1),
  time_period = c("Time 1", "Time 2", "Time 3"),
  cases_std_all = 0,
  deaths_std_all = 0,
  total_population_std = 0,
  POP_SQMI_std = 0,
  median_age_std = 0,
  pct_non_white_std = 0,
  pct_college_deg_std = 0,
  pct_below_pov_lvl_std = 0,
  pct_above_2_pov_lvl_std = 0,
  pct_with_comp_broadband_std = 0,
  pct_speak_only_english_std = 0,
  PCT_LACCESS_POP15_std = 0,
  state_capital = 0,
  dem_pct_std = 0)

# predict outcomes and get estimates
new_data_rec$outcome <- predict.rob(m_recreation, 
                                    vcov_state_county, 
                                    new_data_rec)$fit
# predict outcomes and get se for estimates
new_data_rec$se_outcome <- predict.rob(m_recreation, 
                                       vcov_state_county, 
                                       new_data_rec)$se.fit
# continuous scale time period labels
new_data_rec$time_period_2 <- ifelse(new_data_rec$time_period == "Time 1", "Before March 12", 
                                     ifelse(new_data_rec$time_period == "Time 2", 
                                            "March 12 - Statewide Order", "After Statewide Order"))
new_data_rec$time_period_2 <- factor(new_data_rec$time_period_2,
                                     levels = c("Before March 12",
                                                "March 12 - Statewide Order",
                                                "After Statewide Order"))
# plot predictions
ggplot(new_data_rec %>% 
         mutate(
           lwr = outcome - 1.96 * se_outcome,
           upr = outcome + 1.96 * se_outcome), 
       aes(x = news_desert_rev, y = outcome)) + 
  geom_hline(yintercept = 0, lty = 2) +
  # geom_point() +
  geom_line() +
  geom_errorbar(aes(ymin = lwr, ymax = upr), widht = 0.5) +
  facet_wrap(~ time_period_2) +
  xlab("News Desert Status") + 
  ylab("Predicted Standard-Deviation Change in Mobility") + 
  ggtitle("B. Travel for Retail or Recreation - Unweighted") +
  scale_x_continuous(breaks = c(0, 1),
                     labels = c("Yes", "No")) +
  # scale_color_brewer(name = "Time Period", palette = "Paired") +
  guides(fill = FALSE) +
  fischeR::theme_saf_no_font() + 
  theme(panel.border = element_blank(), # remove borders
        # add x-axis line
        axis.line.x = element_line(),
        axis.line.y = element_line(),
        strip.text = element_text(size = 9,
                                  colour = "black",
                                  family = "Helvetica Neue"),
        strip.background = element_rect(colour = "white",
                                        fill = "white")) -> p2
mobility_reports_2 <-
  google_mobility_2 %>%
  dplyr::filter(!is.na(cases_std_all),
                !is.na(deaths_std_all),
                !is.na(order_date_std),
                !is.na(after_order),
                !is.na(total_population_std),
                !is.na(POP_SQMI_std),
                !is.na(median_age_std),
                !is.na(pct_non_white_std),
                !is.na(pct_college_deg_std),
                !is.na(pct_below_pov_lvl_std),
                !is.na(pct_above_2_pov_lvl_std),
                !is.na(pct_with_comp_broadband_std),
                !is.na(pct_speak_only_english_std),
                !is.na(state_capital),
                !is.na(dem_pct_std),
                !is.na(PCT_LACCESS_POP15_std),
                !is.na(news_desert_rev))

load("data/counties_weights_binary.RData")

# non-parametric approach to covariate balancing propensity score weights
# set.seed(101)
# start_time <- Sys.time()
# cbgps_fit <- npCBPS(
#   news_desert_rev ~
#     total_population_std + # county pop.
#     POP_SQMI_std + # county pop. density
#     median_age_std + # county median age
#     pct_non_white_std + # share of pop not white
#     pct_college_deg_std + # share of pop with any college deg
#     pct_below_pov_lvl_std +  # share of pop below pov line
#     pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
#     pct_with_comp_broadband_std + # share with computer and broadband int
#     pct_speak_only_english_std + # share that only speak english at home
#     PCT_LACCESS_POP15_std + # share with low access to food
#     state_capital + # state capital?
#     dem_pct_std, # partisan lean
#   data = counties, corprior = 0.0001
# )
# end_time <- Sys.time()

# mean correlation for balanced covariates
# mean(abs(balance(cbgps_fit)$balanced))
# mean(abs(balance(cbgps_fit)$unweighted))

# store balanced/unweighted correlations in df
# balance_df <- as.data.frame(
#   rbind(balance(cbgps_fit)$balanced, balance(cbgps_fit)$unweighted)
# )
# balance_df$group <- c(rep("Balanced", nrow(balance_df) / 2), 
#                       rep("Unweighted", nrow(balance_df) / 2))
# balance_df$group <- factor(balance_df$group, 
#                            levels = c("Unweighted", "Balanced"))

# test to see if balancing reduces correlations with treatment
# wilcox.test(balance_df$`Pearson Correlation`[which(balance_df$group == 
# "Balanced")], 
# balance_df$`Pearson Correlation`[which(balance_df$group ==
# "Unweighted")], 
# alternative = "less")

# plot figure 3A
# ggplot(balance_df, aes(x = group, y = `Pearson Correlation`)) +
#   geom_boxplot() +
#   scale_x_discrete("Group") +
#   fischeR::theme_saf_no_font() -> balance_cor_comp

# plot figure 3B
# ggplot() +
#   geom_vline(xintercept = 0, lty = 2) +
#   geom_segment(aes(x = abs(balance(cbgps_fit)$unweighted),
#                    xend = abs(balance(cbgps_fit)$balanced),
#                    y = rownames(balance(cbgps_fit)$balanced),
#                    yend = rownames(balance(cbgps_fit)$balanced)),
#                arrow = arrow(length = unit(0.1, "cm"))) +
#   scale_y_discrete(labels = rev(c("Population",
#                                   "State Capital",
#                                   "Population Density",
#                                   "Pct. w/ Computer and Broadband",
#                                   "Pct. Speaking only English at Home",
#                                   "Pct. Not White",
#                                   "Pct. w/ Low Access to Food",
#                                   "Pct. w/ College Deg.",
#                                   "Pct. Below Poverty Line",
#                                   "Pct. at least 2x Above Poverty Line",
#                                   "Median Age",
#                                   "2016 Clinton Vote Share"))) +
#   ylab("Variable") +
#   xlab("| Correlation |") +
#   fischeR::theme_saf_no_font() -> change_cor_plot

# plot figure 3
# balance_cor_comp + change_cor_plot

# store weights in df
# counties$weights <- cbgps_fit$weights

# merge weights into daily data
mobility_reports_2 <-
  mobility_reports_2 %>%
  inner_join(counties)

# first DiD model with covariate adjustment
m_residential_CBGPS <- lm(residential_std ~ 
                            news_desert_rev * time_period +
                            cases_std_all +
                            deaths_std_all + 
                            total_population_std + # county pop.
                            POP_SQMI_std + # county pop. density
                            median_age_std  + # county median age
                            pct_non_white_std  + # share of pop not white
                            pct_college_deg_std + # share of pop with any college deg
                            pct_below_pov_lvl_std +  # share of pop below pov line
                            pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
                            pct_with_comp_broadband_std + # share with computer and broadband int
                            pct_speak_only_english_std + # share that only speak english at home
                            PCT_LACCESS_POP15_std + # share with low access to food
                            state_capital + # state capital?
                            dem_pct_std,
                          data = mobility_reports_2, weights = weights)
# get clustered standard errors
vcov_state_county_CBGPS <- multiwayvcov::cluster.vcov(m_residential_CBGPS,
                                                      ~ state_abrv + fips)
# summary output using clustered standard errors
lmtest::coeftest(m_residential_CBGPS, vcov = vcov_state_county_CBGPS)
confint(lmtest::coeftest(m_residential_CBGPS, vcov = vcov_state_county_CBGPS))
# generate new data for predictions
new_data_res_CBGPS <- expand.grid(
  news_desert_rev = c(0, 1),
  time_period = c("Time 1", "Time 2", "Time 3"),
  cases_std_all = 0,
  deaths_std_all = 0,
  total_population_std = 0,
  POP_SQMI_std = 0,
  median_age_std = 0,
  pct_non_white_std = 0,
  pct_college_deg_std = 0,
  pct_below_pov_lvl_std = 0,
  pct_above_2_pov_lvl_std = 0,
  pct_with_comp_broadband_std = 0,
  pct_speak_only_english_std = 0,
  PCT_LACCESS_POP15_std = 0,
  state_capital = 0,
  dem_pct_std = 0)

# predict outcomes and get estimates
new_data_res_CBGPS$outcome <- predict.rob(m_residential_CBGPS, 
                                          vcov_state_county_CBGPS, 
                                          new_data_res_CBGPS)$fit
# predict outcomes and get se for estimates
new_data_res_CBGPS$se_outcome <- predict.rob(m_residential_CBGPS, 
                                             vcov_state_county_CBGPS, 
                                             new_data_res_CBGPS)$se.fit
# continuous scale time period labels
new_data_res_CBGPS$time_period_2 <- ifelse(new_data_res_CBGPS$time_period == "Time 1", "Before March 11th", 
                                           ifelse(new_data_res$time_period == "Time 2", 
                                                  "March 12 - Statewide Order", "After Statewide Order"))
new_data_res_CBGPS$time_period_2 <- factor(new_data_res_CBGPS$time_period_2,
                                           levels = c("Before March 11th",
                                                      "March 12 - Statewide Order",
                                                      "After Statewide Order"))

# plot predictions
ggplot(new_data_res_CBGPS %>% 
         mutate(
           lwr = outcome - 1.96 * se_outcome,
           upr = outcome + 1.96 * se_outcome), 
       aes(x = news_desert_rev, y = outcome)) + 
  geom_hline(yintercept = 0, lty = 2) +
  # geom_point() +
  geom_line() +
  geom_errorbar(aes(ymin = lwr, ymax = upr), widht = 0.5) +
  facet_wrap(~ time_period_2) +
  xlab("News Desert Status") + 
  ylab("Predicted Standard-Deviation Change in Mobility") + 
  ggtitle("C. Staying Home - Weighted") +
  scale_x_continuous(breaks = c(0, 1),
                     labels = c("Yes", "No")) +
  # scale_color_brewer(name = "Time Period", palette = "Paired") +
  guides(fill = FALSE) +
  fischeR::theme_saf_no_font() + 
  theme(panel.border = element_blank(), # remove borders
        # add x-axis line
        axis.line.x = element_line(),
        axis.line.y = element_line(),
        strip.text = element_text(size = 9,
                                  colour = "black",
                                  family = "Helvetica Neue"),
        strip.background = element_rect(colour = "white",
                                        fill = "white")) -> p3

m_recreation_CBGPS <- lm(recreation_std ~ 
                           news_desert_rev * time_period +
                           cases_std_all +
                           deaths_std_all + 
                           total_population_std + # county pop.
                           POP_SQMI_std + # county pop. density
                           median_age_std  + # county median age
                           pct_non_white_std  + # share of pop not white
                           pct_college_deg_std + # share of pop with any college deg
                           pct_below_pov_lvl_std +  # share of pop below pov line
                           pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
                           pct_with_comp_broadband_std + # share with computer and broadband int
                           pct_speak_only_english_std + # share that only speak english at home
                           PCT_LACCESS_POP15_std + # share with low access to food
                           state_capital + # state capital?
                           dem_pct_std,
                         data = mobility_reports_2, weights = weights)
vcov_state_county_rec_CBGPS <- multiwayvcov::cluster.vcov(m_recreation_CBGPS,
                                                          ~ state_abrv + fips)
lmtest::coeftest(m_recreation_CBGPS, vcov = vcov_state_county_rec_CBGPS)
confint(lmtest::coeftest(m_recreation_CBGPS, vcov = vcov_state_county_rec_CBGPS))
new_data_rec_CBGPS <- expand.grid(
  news_desert_rev = c(0, 1),
  time_period = c("Time 1", "Time 2", "Time 3"),
  cases_std_all = 0,
  deaths_std_all = 0,
  total_population_std = 0,
  POP_SQMI_std = 0,
  median_age_std = 0,
  pct_non_white_std = 0,
  pct_college_deg_std = 0,
  pct_below_pov_lvl_std = 0,
  pct_above_2_pov_lvl_std = 0,
  pct_with_comp_broadband_std = 0,
  pct_speak_only_english_std = 0,
  PCT_LACCESS_POP15_std = 0,
  state_capital = 0,
  dem_pct_std = 0)

# predict outcomes and get estimates
new_data_rec_CBGPS$outcome <- predict.rob(m_recreation_CBGPS, 
                                          vcov_state_county_CBGPS, 
                                          new_data_rec_CBGPS)$fit
# predict outcomes and get se for estimates
new_data_rec_CBGPS$se_outcome <- predict.rob(m_recreation_CBGPS, 
                                             vcov_state_county_CBGPS, 
                                             new_data_rec_CBGPS)$se.fit
# continuous scale time period labels
new_data_rec_CBGPS$time_period_2 <- ifelse(new_data_rec_CBGPS$time_period == "Time 1", "Before March 11th", 
                                           ifelse(new_data_rec_CBGPS$time_period == "Time 2", 
                                                  "March 12 - Statewide Order", "After Statewide Order"))
new_data_rec_CBGPS$time_period_2 <- factor(new_data_rec_CBGPS$time_period_2,
                                           levels = c("Before March 11th",
                                                      "March 12 - Statewide Order",
                                                      "After Statewide Order"))
# plot predictions
ggplot(new_data_rec_CBGPS %>% 
         mutate(
           lwr = outcome - 1.96 * se_outcome,
           upr = outcome + 1.96 * se_outcome), 
       aes(x = news_desert_rev, y = outcome)) + 
  geom_hline(yintercept = 0, lty = 2) +
  # geom_point() +
  geom_line() +
  geom_errorbar(aes(ymin = lwr, ymax = upr), widht = 0.5) +
  facet_wrap(~ time_period_2) +
  xlab("News Desert Status") + 
  ylab("Predicted Standard-Deviation Change in Mobility") + 
  ggtitle("D. Travel for Retail or Recreation - Weighted") +
  scale_x_continuous(breaks = c(0, 1),
                     labels = c("Yes", "No")) +
  # scale_color_brewer(name = "Time Period", palette = "Paired") +
  guides(fill = FALSE) +
  fischeR::theme_saf_no_font() + 
  theme(panel.border = element_blank(), # remove borders
        # add x-axis line
        axis.line.x = element_line(),
        axis.line.y = element_line(),
        strip.text = element_text(size = 9,
                                  colour = "black",
                                  family = "Helvetica Neue"),
        strip.background = element_rect(colour = "white",
                                        fill = "white")) -> p4

((p1 + 
    theme(axis.title = element_blank(), 
          axis.text.x = element_blank(),
          title = element_text(size = 12))) + 
    (p2 + 
       theme(axis.title = element_blank(), 
             axis.text.x = element_blank(),
             title = element_text(size = 12)))) / 
  ((p3 + 
      theme(strip.text = element_blank(),
            title = element_text(size = 12))) + 
     (p4 + 
        theme(axis.title = element_blank(), 
              strip.text = element_blank(),
              title = element_text(size = 12))))

vcov_state_county_rec <- multiwayvcov::cluster.vcov(m_residential,
                                                    ~ state_abrv + fips)
tab_out <- lmtest::coeftest(m_residential, vcov = vcov_state_county_rec)
tab_out <- as.data.frame(tab_out[, c(1, 2, 4)])
colnames(tab_out) <- c("b", "s.e.", "p")
tab_out_confint <- confint(lmtest::coeftest(m_residential, 
                                            vcov = vcov_state_county_rec))
tab_out_confint <- paste("$", signif(tab_out_confint[, 1], 2), "$, $", 
                         signif(tab_out_confint[, 2], 2), "$",
                         sep = "")
tab_out$`95% CI` <- tab_out_confint
rownames(tab_out) <- c("Intercept", "News Desert (binary)", 
                       "Time Period 2",
                       "Time Period 3",
                       "Cases", "Deaths",
                       "Population Size",
                       "Population Density",
                       "Median Age",
                       "Pct. Not White",
                       "Pct. w/ College Degree",
                       "Pct. Below Povtery Level",
                       "Pct. at least 2x Above Poverty Level",
                       "Pct. w/ Computer and Broadband",
                       "Pct. Speak only English at Home",
                       "Pct. w/ Low Access to Food",
                       "State Capital",
                       "2016 Clinton Vote Share",
                       "Total Papers : Time Period 2",
                       "Total Papers : Time Period 3")
tab_out$Var <- rownames(tab_out)
tab_out$b <- signif(tab_out$b, 2)
tab_out$`s.e.` <- signif(tab_out$`s.e.`, 2)
tab_out$p <- signif(tab_out$p, 2)

vcov_state_county_rec <- multiwayvcov::cluster.vcov(m_residential_CBGPS,
                                                    ~ state_abrv + fips)
tab_out2 <- lmtest::coeftest(m_residential_CBGPS, vcov = vcov_state_county_rec)
tab_out2 <- as.data.frame(tab_out2[, c(1, 2, 4)])
colnames(tab_out2) <- c("b", "s.e.", "p")
tab_out2_confint <- confint(lmtest::coeftest(m_residential_CBGPS, 
                                             vcov = vcov_state_county_rec))
tab_out2_confint <- paste("$", signif(tab_out2_confint[, 1], 2), "$, $", 
                          signif(tab_out2_confint[, 2], 2), "$",
                          sep = "")
tab_out2$`95% CI` <- tab_out2_confint
rownames(tab_out2) <- c("Intercept", "News Desert (binary)", 
                        "Time Period 2",
                        "Time Period 3",
                        "Cases", "Deaths",
                        "Population Size",
                        "Population Density",
                        "Median Age",
                        "Pct. Not White",
                        "Pct. w/ College Degree",
                        "Pct. Below Povtery Level",
                        "Pct. at least 2x Above Poverty Level",
                        "Pct. w/ Computer and Broadband",
                        "Pct. Speak only English at Home",
                        "Pct. w/ Low Access to Food",
                        "State Capital",
                        "2016 Clinton Vote Share",
                        "Total Papers : Time Period 2",
                        "Total Papers : Time Period 3")
tab_out2$Var <- rownames(tab_out2)
tab_out2$b <- signif(tab_out2$b, 2)
tab_out2$`s.e.` <- signif(tab_out2$`s.e.`, 2)
tab_out2$p <- signif(tab_out2$p, 2)

tab_out3 <-
  tab_out %>%
  full_join(tab_out2, by = c("Var" = "Var"))
rownames(tab_out3) <- tab_out3$Var
tab_out3 <- tab_out3[, -5]

stargazer::stargazer(tab_out3, summary = FALSE, digits = NA)

vcov_state_county_rec <- multiwayvcov::cluster.vcov(m_recreation,
                                                    ~ state_abrv + fips)
tab_out4 <- lmtest::coeftest(m_recreation, vcov = vcov_state_county_rec)
tab_out4 <- as.data.frame(tab_out4[, c(1, 2, 4)])
colnames(tab_out4) <- c("b", "s.e.", "p")
tab_out4_confint <- confint(lmtest::coeftest(m_recreation, 
                                             vcov = vcov_state_county_rec))
tab_out4_confint <- paste("$", signif(tab_out4_confint[, 1], 2), "$, $", 
                          signif(tab_out4_confint[, 2], 2), "$",
                          sep = "")
tab_out4$`95% CI` <- tab_out4_confint
rownames(tab_out4) <- c("Intercept", "News Desert (binary)", 
                        "Time Period 2",
                        "Time Period 3",
                        "Cases", "Deaths",
                        "Population Size",
                        "Population Density",
                        "Median Age",
                        "Pct. Not White",
                        "Pct. w/ College Degree",
                        "Pct. Below Povtery Level",
                        "Pct. at least 2x Above Poverty Level",
                        "Pct. w/ Computer and Broadband",
                        "Pct. Speak only English at Home",
                        "Pct. w/ Low Access to Food",
                        "State Capital",
                        "2016 Clinton Vote Share",
                        "Total Papers : Time Period 2",
                        "Total Papers : Time Period 3")
tab_out4$Var <- rownames(tab_out4)
tab_out4$b <- signif(tab_out4$b, 2)
tab_out4$`s.e.` <- signif(tab_out4$`s.e.`, 2)
tab_out4$p <- signif(tab_out4$p, 2)

vcov_state_county_rec <- multiwayvcov::cluster.vcov(m_recreation_CBGPS,
                                                    ~ state_abrv + fips)
tab_out5 <- lmtest::coeftest(m_recreation_CBGPS, vcov = vcov_state_county_rec)
tab_out5 <- as.data.frame(tab_out5[, c(1, 2, 4)])
colnames(tab_out5) <- c("b", "s.e.", "p")
tab_out5_confint <- confint(lmtest::coeftest(m_recreation_CBGPS, 
                                             vcov = vcov_state_county_rec))
tab_out5_confint <- paste("$", signif(tab_out5_confint[, 1], 2), "$, $", 
                          signif(tab_out5_confint[, 2], 2), "$",
                          sep = "")
tab_out5$`95% CI` <- tab_out5_confint
rownames(tab_out5) <- c("Intercept", "News Desert (binary)", 
                        "Time Period 2",
                        "Time Period 3",
                        "Cases", "Deaths",
                        "Population Size",
                        "Population Density",
                        "Median Age",
                        "Pct. Not White",
                        "Pct. w/ College Degree",
                        "Pct. Below Povtery Level",
                        "Pct. at least 2x Above Poverty Level",
                        "Pct. w/ Computer and Broadband",
                        "Pct. Speak only English at Home",
                        "Pct. w/ Low Access to Food",
                        "State Capital",
                        "2016 Clinton Vote Share",
                        "Total Papers : Time Period 2",
                        "Total Papers : Time Period 3")
tab_out5$Var <- rownames(tab_out5)
tab_out5$b <- signif(tab_out5$b, 2)
tab_out5$`s.e.` <- signif(tab_out5$`s.e.`, 2)
tab_out5$p <- signif(tab_out5$p, 2)

tab_out6 <-
  tab_out4 %>%
  full_join(tab_out5, by = c("Var" = "Var"))
rownames(tab_out6) <- tab_out6$Var
tab_out6 <- tab_out6[, -5]

stargazer::stargazer(tab_out6, summary = FALSE)
