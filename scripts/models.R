library(lfe)
library(ggplot2)
library(patchwork)
library(CBPS)

source("scripts/merge_data.R")
mobility_reports <-
  mobility_reports %>%
  dplyr::select(state, state_2, county, fips,
         residential, retail_recreation,
         paper_count_std, paper_count_log_std, news_desert_rev,
         cases, cases_std, deaths, deaths_std,
         before_mar_29, days_since_order_2, days_since_order_2_std,
         total_population, total_population_std,
         POP_SQMI, POP_SQMI_std,
         median_age, median_age_std,
         pct_non_white, pct_non_white_std,
         pct_college_deg, pct_college_deg_std,
         pct_below_pov_lvl, pct_below_pov_lvl_std,
         pct_above_2_pov_lvl, pct_above_2_pov_lvl_std,
         pct_with_comp_broadband, pct_with_comp_broadband_std,
         pct_speak_only_english, pct_speak_only_english_std,
         PCT_LACCESS_POP15, PCT_LACCESS_POP15_std,
         state_capital,
         dem_pct, dem_pct_std)


m_residential <-
  felm(residential ~ paper_count_std | 0 | 0 | state, data = mobility_reports)
summary(m_residential)
confint(m_residential)

m_recreation <-
  felm(retail_recreation ~ paper_count_std | 0 | 0 | state, 
       data = mobility_reports)
summary(m_recreation)
confint(m_recreation)

m0_residential <-
  felm(residential ~ paper_count_std + cases_std + deaths_std + 
        before_mar_29 + days_since_order_2_std | 0 | 0 | state, 
       data = mobility_reports)
summary(m0_residential)
confint(m0_residential)

m0_recreation <-
  felm(retail_recreation ~ paper_count_std + cases_std + deaths_std +
         before_mar_29 + days_since_order_2_std | 0 | 0 | state, 
       data = mobility_reports)
summary(m0_recreation)
confint(m0_recreation)

to_plot <- as.data.frame(summary(m0_residential)$coefficients)
to_plot$Term <- rownames(to_plot)
to_plot <-
  to_plot %>%
  filter(Term != "(Intercept)")
to_plot$Term <- factor(to_plot$Term, levels = to_plot$Term)
to_plot$sig <- ifelse(to_plot$`Pr(>|t|)` < 0.05, "Yes", "No")


# produce figure 3
p1 <- 
  ggplot(to_plot, aes(x = Estimate, y = Term, shape = sig)) +
  geom_point(size = 2.25) +
  geom_errorbarh(aes(xmin = (Estimate - 1.96 * `Cluster s.e.`), 
                     xmax = (Estimate + 1.96 * `Cluster s.e.`)),
                 height = 0.5) +
  geom_vline(xintercept = 0, lty = 2) +
  scale_y_discrete(limits = rev(levels(to_plot$Term)),
                   labels = rev(c("Total Papers", "Cases", 
                                  "Deaths", "Stay-at-Home Order",
                                  "Days since Order"
                   ))) +
  scale_shape_manual(values = c(1, 19)) +
  guides(shape = FALSE) +
  ylab("Term") +
  ggtitle("Residential") +
  fischeR::theme_saf_no_font() +
  theme(axis.text = element_text(size = 12),
        title = element_text(size = 14))


to_plot_2 <- as.data.frame(summary(m0_recreation)$coefficients)
to_plot_2$Term <- rownames(to_plot_2)
to_plot_2 <-
  to_plot_2 %>%
  filter(Term != "(Intercept)")
to_plot_2$Term <- factor(to_plot_2$Term, levels = to_plot_2$Term)
to_plot_2$sig <- ifelse(to_plot_2$`Pr(>|t|)` < 0.05, "Yes", "No")


# produce figure 3
p2 <-
  ggplot(to_plot_2, aes(x = Estimate, y = Term, shape = sig)) +
  geom_point(size = 2.25) +
  geom_errorbarh(aes(xmin = (Estimate - 1.96 * `Cluster s.e.`), 
                     xmax = (Estimate + 1.96 * `Cluster s.e.`)),
                 height = 0.5) +
  geom_vline(xintercept = 0, lty = 2) +
  scale_y_discrete(limits = rev(levels(to_plot_2$Term)),
                   labels = rev(c("Total Papers", "Cases", 
                                  "Deaths", "Stay-at-Home Order",
                                  "Days since Order"
                   ))) +
  scale_shape_manual(values = c(1, 19)) +
  guides(shape = FALSE) +
  ylab("Term") +
  ggtitle("Retail and Recreation") +
  fischeR::theme_saf_no_font() +
  theme(axis.text = element_text(size = 12),
        title = element_text(size = 14))

(p1 + theme(axis.text.y = element_text(size = 10))) + 
  (p2 + theme(axis.text.y = element_blank(), 
              axis.title = element_blank()))

m1_residential <- 
  felm(residential ~ 
         paper_count_std +
         cases_std + # cases on Mar 29
         deaths_std + # deaths on Mar 29
         before_mar_29 +
         days_since_order_2_std + # stay at home order by Mar 29
         total_population_std + # county pop.
         POP_SQMI_std + # county pop. density
         median_age_std +
         pct_non_white_std + # share of pop not white
         pct_college_deg_std + # share of pop with any college deg
         pct_below_pov_lvl_std +  # share of pop below pov line
         pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
         pct_with_comp_broadband_std + # share with computer and broadband int
         pct_speak_only_english_std + # share that only speak english at home
         PCT_LACCESS_POP15_std + # share with low access to food
         state_capital + # state capital?
         dem_pct_std # partisan lean
          | 0 | 0 | state, 
       data = mobility_reports)
summary(m1_residential)
confint(m1_residential)

m1_recreation <-
  felm(retail_recreation ~
         paper_count_std +
         cases_std + # cases on Mar 29
         deaths_std + # deaths on Mar 29
         before_mar_29 +
         days_since_order_2_std + # stay at home order by Mar 29
         total_population_std + # county pop.
         POP_SQMI_std + # county pop. density
         median_age_std +
         pct_non_white_std + # share of pop not white
         pct_college_deg_std + # share of pop with any college deg
         pct_below_pov_lvl_std +  # share of pop below pov line
         pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
         pct_with_comp_broadband_std + # share with computer and broadband int
         pct_speak_only_english_std + # share that only speak english at home
         PCT_LACCESS_POP15_std + # share with low access to food
         state_capital + # state capital?
         dem_pct_std # partisan lean
          | 0 | 0 | state, 
       data = mobility_reports)
summary(m1_recreation)
confint(m1_recreation)

to_plot <- as.data.frame(summary(m1_residential)$coefficients)
to_plot$Term <- rownames(to_plot)
to_plot <-
  to_plot %>%
  filter(Term != "(Intercept)")
to_plot$Term <- factor(to_plot$Term, levels = to_plot$Term)
to_plot$sig <- ifelse(to_plot$`Pr(>|t|)` < 0.05, "Yes", "No")

# produce figure 3
p3 <- 
  ggplot(to_plot, aes(x = Estimate, y = Term, shape = sig)) +
  geom_point(size = 2.25) +
  geom_errorbarh(aes(xmin = (Estimate - 1.96 * `Cluster s.e.`), 
                     xmax = (Estimate + 1.96 * `Cluster s.e.`)),
                 height = 0.5) +
  geom_vline(xintercept = 0, lty = 2) +
  scale_y_discrete(limits = rev(levels(to_plot$Term)),
                   labels = rev(c("Total Papers", "Cases", 
                                  "Deaths", "Stay-at-Home Order",
                                  "Days since Order", "Population",
                                  "Population Density", "Median Age",
                                  "Pct. Not White", "Pct. w/ College Deg.",
                                  "Pct. Below Poverty Line",
                                  "Pct. at least 2x Above Poverty Line",
                                  "Pct. w/ Computer and Broadband",
                                  "Pct. Speaking only English at Home",
                                  "Pct. w/ Low Access to Food",
                                  "State Capital",
                                  "2016 Clinton Vote Share"
                   ))) +
  scale_shape_manual(values = c(1, 19)) +
  guides(shape = FALSE) +
  ylab("Term") +
  ggtitle("Residential") +
  fischeR::theme_saf_no_font() +
  theme(axis.text = element_text(size = 12),
        title = element_text(size = 14))


to_plot_2 <- as.data.frame(summary(m1_recreation)$coefficients)
to_plot_2$Term <- rownames(to_plot_2)
to_plot_2 <-
  to_plot_2 %>%
  filter(Term != "(Intercept)")
to_plot_2$Term <- factor(to_plot_2$Term, levels = to_plot_2$Term)
to_plot_2$sig <- ifelse(to_plot_2$`Pr(>|t|)` < 0.05, "Yes", "No")

# produce figure 3
p4 <-
  ggplot(to_plot_2, aes(x = Estimate, y = Term, shape = sig)) +
  geom_point(size = 2.25) +
  geom_errorbarh(aes(xmin = (Estimate - 1.96 * `Cluster s.e.`), 
                     xmax = (Estimate + 1.96 * `Cluster s.e.`)),
                 height = 0.5) +
  geom_vline(xintercept = 0, lty = 2) +
  scale_y_discrete(limits = rev(levels(to_plot_2$Term)),
                   labels = rev(c("Total Papers", "Cases", 
                                  "Deaths", "Stay-at-Home Order",
                                  "Days since Order", "Population",
                                  "Population Density", "Median Age",
                                  "Pct. Not White", "Pct. w/ College Deg.",
                                  "Pct. Below Poverty Line",
                                  "Pct. at least 2x Above Poverty Line",
                                  "Pct. w/ Computer and Broadband",
                                  "Pct. Speaking only English at Home",
                                  "Pct. w/ Low Access to Food",
                                  "State Capital",
                                  "2016 Clinton Vote Share"
                   ))) +
  scale_shape_manual(values = c(1, 19)) +
  guides(shape = FALSE) +
  ylab("Term") +
  ggtitle("Retail and Recreation") +
  fischeR::theme_saf_no_font() +
  theme(axis.text = element_text(size = 12),
        title = element_text(size = 14))

(p3 + theme(axis.text.y = element_text(size = 10))) + 
  (p4 + theme(axis.text.y = element_blank(), 
              axis.title = element_blank()))

# m1_sensitivity <-
#   felm(paper_count_std ~
#          cases_std + # cases on Mar 29
#          deaths_std + # deaths on Mar 29
#          days_since_order_2_std + # stay at home order by Mar 29
#          total_population_std + # county pop.
#          POP_SQMI_std + # county pop. density
#          pct_non_white_std + # share of pop not white
#          # pct_high_school_ged_std + 
#          pct_college_deg_std + # share of pop with any college deg
#          pct_below_pov_lvl_std +  # share of pop below pov line
#          pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
#          pct_with_comp_broadband_std + # share with computer and broadband int
#          # pct_with_insurance_std +
#          pct_speak_only_english_std + # share that only speak english at home 
#          state_capital + # state capital?
#          dem_pct_std + # partisan lean
#          # turnout_16_std + # estimated relative turnout in 2016 presidential
#          # response_final_std + # 2010 census turnout
#          PCT_LACCESS_POP15_std # share with low access to food
#           | 0 | 0 | state, 
#        data = mobility_reports)
# summary(m1_sensitivity)

# m2_residential <- 
#   felm(residential ~ 
#          cases_std + 
#          deaths_std + 
#          pct_non_white_std +
#          pct_high_school_ged_std + pct_college_deg_std +
#          pct_below_pov_lvl_std + pct_above_2_pov_lvl_std + 
#          pct_with_comp_broadband_std +
#          pct_with_insurance_std + pct_speak_only_english_std + state_capital +
#          dem_pct_std + turnout_16_std + PCT_LACCESS_POP15_std + POP_SQMI_std + 
#          response_final_std +
#          paper_count_std | 0 | 0 | state, 
#        data = mobility_reports %>% filter(days_since_order_2_std == 1))
# summary(m2_residential)

# m2_recreation <-
#   felm(retail_recreation ~
#          cases_std + 
#          deaths_std + 
#          pct_non_white_std +
#          pct_high_school_ged_std + pct_college_deg_std +
#          pct_below_pov_lvl_std + pct_above_2_pov_lvl_std + 
#          pct_with_comp_broadband_std +
#          pct_with_insurance_std + pct_speak_only_english_std + state_capital +
#          dem_pct_std + turnout_16_std + PCT_LACCESS_POP15_std + POP_SQMI_std + 
#          response_final_std +
#          paper_count_std | 0 | 0 | state, 
#        data = mobility_reports %>% filter(days_since_order_2_std == 1))
# summary(m2_recreation)

# m3_residential <- 
#   felm(residential ~ 
#          cases_std + 
#          deaths_std + 
#          pct_non_white_std +
#          pct_high_school_ged_std + pct_college_deg_std +
#          pct_below_pov_lvl_std + pct_above_2_pov_lvl_std + 
#          pct_with_comp_broadband_std +
#          pct_with_insurance_std + pct_speak_only_english_std + state_capital +
#          dem_pct_std + turnout_16_std + PCT_LACCESS_POP15_std + POP_SQMI_std + 
#          response_final_std +
#          paper_count_std | 0 | 0 | state, 
#        data = mobility_reports %>% filter(days_since_order_2_std == 0))
# summary(m3_residential)

# m3_recreation <-
#   felm(retail_recreation ~
#          cases_std + 
#          deaths_std + 
#          pct_non_white_std +
#          pct_high_school_ged_std + pct_college_deg_std +
#          pct_below_pov_lvl_std + pct_above_2_pov_lvl_std + 
#          pct_with_comp_broadband_std +
#          pct_with_insurance_std + pct_speak_only_english_std + state_capital +
#          dem_pct_std + turnout_16_std + PCT_LACCESS_POP15_std + POP_SQMI_std + 
#          response_final_std +
#          paper_count| 0 | 0 | state, 
#        data = mobility_reports %>% filter(days_since_order_2_std == 0))
# summary(m3_recreation)

# m4_residential <- 
#   felm(residential ~ 
#          pct_non_white_std +
#          pct_high_school_ged_std + pct_college_deg_std +
#          pct_below_pov_lvl_std + pct_above_2_pov_lvl_std + 
#          pct_with_comp_broadband_std +
#          pct_with_insurance_std + pct_speak_only_english_std + 
#          days_since_order_2_std + state_capital +
#          dem_pct_std + turnout_16_std + PCT_LACCESS_POP15_std + POP_SQMI_std + 
#          response_final_std +
#          paper_count_std | 0 | 0 | state, 
#        data = mobility_reports %>% filter(cases != 0))
# summary(m4_residential)

# m4_recreation <-
#   felm(retail_recreation ~
#          cases_std +
#          pct_non_white_std +
#          pct_high_school_ged_std + pct_college_deg_std +
#          pct_below_pov_lvl_std + pct_above_2_pov_lvl_std + 
#          pct_with_comp_broadband_std +
#          pct_with_insurance_std + pct_speak_only_english_std + 
#          days_since_order_2_std + state_capital +
#          dem_pct_std + turnout_16_std + PCT_LACCESS_POP15_std + POP_SQMI_std + 
#          response_final_std +
#          paper_count_std | 0 | 0 | state, 
#        data = mobility_reports %>% filter(cases != 0))
# summary(m4_recreation)

# m5_residential <- 
#   felm(residential ~ 
#          pct_non_white_std +
#          pct_high_school_ged_std + pct_college_deg_std +
#          pct_below_pov_lvl_std + pct_above_2_pov_lvl_std + 
#          pct_with_comp_broadband_std +
#          pct_with_insurance_std + pct_speak_only_english_std + state_capital +
#          dem_pct_std + turnout_16_std + PCT_LACCESS_POP15_std + POP_SQMI_std + 
#          response_final_std +
#          paper_count_std | 0 | 0 | state, 
#        data = mobility_reports %>% filter(cases == 0))
# summary(m5_residential)

# m5_recreation <-
#   felm(retail_recreation ~
#          pct_non_white_std +
#          pct_high_school_ged_std + pct_college_deg_std +
#          pct_below_pov_lvl_std + pct_above_2_pov_lvl_std + 
#          pct_with_comp_broadband_std +
#          pct_with_insurance_std + pct_speak_only_english_std + state_capital +
#          dem_pct_std + turnout_16_std + PCT_LACCESS_POP15_std + POP_SQMI_std + 
#          response_final_std +
#          paper_count_std | 0 | 0 | state, 
#        data = mobility_reports %>% filter(cases == 0))
# summary(m5_recreation)

mobility_reports_2 <-
  mobility_reports %>%
  filter(!is.na(cases_std),
         !is.na(deaths_std),
         !is.na(days_since_order_2_std),
         !is.na(total_population_std),
         !is.na(POP_SQMI_std),
         !is.na(median_age_std),
         !is.na(pct_non_white_std),
         !is.na(pct_college_deg_std),
         !is.na(pct_below_pov_lvl_std),
         !is.na(pct_above_2_pov_lvl_std),
         !is.na(pct_with_comp_broadband_std),
         !is.na(pct_speak_only_english_std),
         !is.na(state_capital),
         !is.na(dem_pct_std),
         !is.na(PCT_LACCESS_POP15_std),
         !is.na(paper_count_std))

cbgps_fit <- npCBPS(
  paper_count_std ~
    cases_std + # cases on Mar 29
    deaths_std + # deaths on Mar 29
    before_mar_29 +
    days_since_order_2_std + # stay at home order by Mar 29
    total_population_std + # county pop.
    POP_SQMI_std + # county pop. density
    median_age_std +
    pct_non_white_std + # share of pop not white
    pct_college_deg_std + # share of pop with any college deg
    pct_below_pov_lvl_std +  # share of pop below pov line
    pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
    pct_with_comp_broadband_std + # share with computer and broadband int
    pct_speak_only_english_std + # share that only speak english at home
    PCT_LACCESS_POP15_std + # share with low access to food
    state_capital + # state capital?
    dem_pct_std, # partisan lean
  data = mobility_reports_2, corprior = 5e-10
)
mean(balance(cbgps_fit)$balanced)
balance_df <- as.data.frame(
  rbind(balance(cbgps_fit)$balanced, balance(cbgps_fit)$unweighted)
)
balance_df$group <- c(rep("Balanced", nrow(balance_df) / 2), 
                      rep("Unweighted", nrow(balance_df) / 2))
balance_df$group <- factor(balance_df$group, 
                           levels = c("Unweighted", "Balanced"))
wilcox.test(balance_df$`Pearson Correlation`[which(balance_df$group == 
                                                     "Balanced")], 
            balance_df$`Pearson Correlation`[which(balance_df$group ==
                                                     "Unweighted")], 
            alternative = "less")
ggplot(balance_df, aes(x = group, y = `Pearson Correlation`)) +
  geom_boxplot() +
  scale_x_discrete("Group") +
  fischeR::theme_saf_no_font()


mobility_reports_2$weights <- cbgps_fit$weights

m1_residential_CBGPS <- 
  felm(residential ~ 
         paper_count_std +
         cases_std + # cases on Mar 29
         deaths_std + # deaths on Mar 29
         before_mar_29 +
         days_since_order_2_std + # stay at home order by Mar 29
         total_population_std + # county pop.
         POP_SQMI_std + # county pop. density
         median_age_std +
         pct_non_white_std + # share of pop not white
         pct_college_deg_std + # share of pop with any college deg
         pct_below_pov_lvl_std +  # share of pop below pov line
         pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
         pct_with_comp_broadband_std + # share with computer and broadband int
         pct_speak_only_english_std + # share that only speak english at home
         PCT_LACCESS_POP15_std + # share with low access to food
         state_capital + # state capital?
         dem_pct_std # partisan lean
          | 0 | 0 | state, 
       data = mobility_reports_2, weights = mobility_reports_2$weights)
summary(m1_residential_CBGPS)
confint(m1_residential_CBGPS)

m1_recreation_CBGPS <- 
  felm(retail_recreation ~ 
         paper_count_std +
         cases_std + # cases on Mar 29
         deaths_std + # deaths on Mar 29
         before_mar_29 +
         days_since_order_2_std + # stay at home order by Mar 29
         total_population_std + # county pop.
         POP_SQMI_std + # county pop. density
         median_age_std +
         pct_non_white_std + # share of pop not white
         pct_college_deg_std + # share of pop with any college deg
         pct_below_pov_lvl_std +  # share of pop below pov line
         pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
         pct_with_comp_broadband_std + # share with computer and broadband int
         pct_speak_only_english_std + # share that only speak english at home
         PCT_LACCESS_POP15_std + # share with low access to food
         state_capital + # state capital?
         dem_pct_std # partisan lean
          | 0 | 0 | state, 
       data = mobility_reports_2, weights = mobility_reports_2$weights)
summary(m1_recreation_CBGPS)
confint(m1_recreation_CBGPS)

to_plot <- as.data.frame(summary(m1_residential_CBGPS)$coefficients)
to_plot$Term <- rownames(to_plot)
to_plot <-
  to_plot %>%
  filter(Term != "(Intercept)")
to_plot$Term <- factor(to_plot$Term, levels = to_plot$Term)
to_plot$sig <- ifelse(to_plot$`Pr(>|t|)` < 0.05, "Yes", "No")

# produce figure 3
p5 <- 
  ggplot(to_plot, aes(x = Estimate, y = Term, shape = sig)) +
  geom_point(size = 2.25) +
  geom_errorbarh(aes(xmin = (Estimate - 1.96 * `Cluster s.e.`), 
                     xmax = (Estimate + 1.96 * `Cluster s.e.`)),
                 height = 0.5) +
  geom_vline(xintercept = 0, lty = 2) +
  scale_y_discrete(limits = rev(levels(to_plot$Term)),
                   labels = rev(c("Total Papers", "Cases", 
                                  "Deaths", "Stay-at-Home Order",
                                  "Days since Order", "Population",
                                  "Population Density", "Median Age",
                                  "Pct. Not White", "Pct. w/ College Deg.",
                                  "Pct. Below Poverty Line",
                                  "Pct. at least 2x Above Poverty Line",
                                  "Pct. w/ Computer and Broadband",
                                  "Pct. Speaking only English at Home",
                                  "Pct. w/ Low Access to Food",
                                  "State Capital",
                                  "2016 Clinton Vote Share"
                                  ))) +
  scale_shape_manual(values = c(1, 19)) +
  guides(shape = FALSE) +
  ylab("Term") +
  ggtitle("Residential") +
  fischeR::theme_saf_no_font() +
  theme(axis.text = element_text(size = 12),
        title = element_text(size = 14))


to_plot_2 <- as.data.frame(summary(m1_recreation_CBGPS)$coefficients)
to_plot_2$Term <- rownames(to_plot_2)
to_plot_2 <-
  to_plot_2 %>%
  filter(Term != "(Intercept)")
to_plot_2$Term <- factor(to_plot_2$Term, levels = to_plot_2$Term)
to_plot_2$sig <- ifelse(to_plot_2$`Pr(>|t|)` < 0.05, "Yes", "No")

# produce figure 3
p6 <-
  ggplot(to_plot_2, aes(x = Estimate, y = Term, shape = sig)) +
  geom_point(size = 2.25) +
  geom_errorbarh(aes(xmin = (Estimate - 1.96 * `Cluster s.e.`), 
                     xmax = (Estimate + 1.96 * `Cluster s.e.`)),
                 height = 0.5) +
  geom_vline(xintercept = 0, lty = 2) +
  scale_y_discrete(limits = rev(levels(to_plot_2$Term)),
                   labels = rev(c("Total Papers", "Cases", 
                                  "Deaths", "Stay-at-Home Order",
                                  "Days since Order", "Population",
                                  "Population Density", "Median Age",
                                  "Pct. Not White", "Pct. w/ College Deg.",
                                  "Pct. Below Poverty Line",
                                  "Pct. at least 2x Above Poverty Line",
                                  "Pct. w/ Computer and Broadband",
                                  "Pct. Speaking only English at Home",
                                  "Pct. w/ Low Access to Food",
                                  "State Capital",
                                  "2016 Clinton Vote Share"
                   ))) +
  scale_shape_manual(values = c(1, 19)) +
  guides(shape = FALSE) +
  ylab("Term") +
  ggtitle("Retail and Recreation") +
  fischeR::theme_saf_no_font() +
  theme(axis.text = element_text(size = 12),
        title = element_text(size = 14))

(p5 + theme(axis.text.y = element_text(size = 10))) + 
  (p6 + theme(axis.text.y = element_blank(), 
              axis.title = element_blank()))

m2_residential_CBGPS <- 
  felm(residential ~ 
         paper_count_std +
         cases_std + # cases on Mar 29
         deaths_std + # deaths on Mar 29
         before_mar_29 +
         days_since_order_2_std + # stay at home order by Mar 29
         total_population_std + # county pop.
         POP_SQMI_std + # county pop. density
         median_age_std +
         pct_non_white_std + # share of pop not white
         pct_college_deg_std + # share of pop with any college deg
         pct_below_pov_lvl_std +  # share of pop below pov line
         pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
         pct_with_comp_broadband_std + # share with computer and broadband int
         pct_speak_only_english_std + # share that only speak english at home
         PCT_LACCESS_POP15_std + # share with low access to food
         state_capital + # state capital?
         dem_pct_std + # partisan lean
         paper_count_std * dem_pct_std
         # paper_count_std * before_mar_29
         # paper_count_std * pct_with_comp_broadband_std 
       | 0 | 0 | state, 
       data = mobility_reports_2, weights = mobility_reports_2$weights)
summary(m2_residential_CBGPS)
confint(m2_residential_CBGPS)

m2_recreation_CBGPS <- 
  felm(retail_recreation ~ 
         paper_count_std +
         cases_std + # cases on Mar 29
         deaths_std + # deaths on Mar 29
         before_mar_29 +
         days_since_order_2_std + # stay at home order by Mar 29
         total_population_std + # county pop.
         POP_SQMI_std + # county pop. density
         median_age_std +
         pct_non_white_std + # share of pop not white
         pct_college_deg_std + # share of pop with any college deg
         pct_below_pov_lvl_std +  # share of pop below pov line
         pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
         pct_with_comp_broadband_std + # share with computer and broadband int
         pct_speak_only_english_std + # share that only speak english at home
         PCT_LACCESS_POP15_std + # share with low access to food
         state_capital + # state capital?
         dem_pct_std + # partisan lean
         paper_count_std * dem_pct_std +
         paper_count_std * before_mar_29 +
         paper_count_std * pct_with_comp_broadband_std
       | 0 | 0 | state, 
       data = mobility_reports_2, weights = mobility_reports_2$weights)
summary(m2_recreation_CBGPS)
confint(m2_recreation_CBGPS)

stargazer::stargazer(m_residential, 
                     m0_residential, 
                     m1_residential, 
                     m1_residential_CBGPS,
                     align = TRUE,
                     no.space = TRUE,
                     covariate.labels = c("Total Papers",
                                          "Cases",
                                          "Deaths",
                                          "Stay-at-Home Order (binary)",
                                          "Days since Order",
                                          "Population Size",
                                          "Population Density",
                                          "Median Age",
                                          "Pct. Not White",
                                          "Pct. w/ College Degree",
                                          "Pct. Below Povtery Level",
                                          "Pct. at least 2x Above Poverty Level",
                                          "Pct. w/ Computer and Broadband",
                                          "Pct. Speak only English at Home",
                                          "Pct. w/ Low Access to Food",
                                          "State Capital",
                                          "2016 Clinton Vote Share"))

stargazer::stargazer(m_recreation, 
                     m0_recreation, 
                     m1_recreation, 
                     m1_recreation_CBGPS,
                     align = TRUE,
                     no.space = TRUE,
                     covariate.labels = c("Total Papers",
                                          "Cases",
                                          "Deaths",
                                          "Stay-at-Home Order (binary)",
                                          "Days since Order",
                                          "Population Size",
                                          "Population Density",
                                          "Median Age",
                                          "Pct. Not White",
                                          "Pct. w/ College Degree",
                                          "Pct. Below Povtery Level",
                                          "Pct. at least 2x Above Poverty Level",
                                          "Pct. w/ Computer and Broadband",
                                          "Pct. Speak only English at Home",
                                          "Pct. w/ Low Access to Food",
                                          "State Capital",
                                          "2016 Clinton Vote Share"))
