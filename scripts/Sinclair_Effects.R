load("~/Downloads/localnews_replication/data/clean/tveyes_stations.RData")

stations <-
  stations %>%
  mutate(countyfips = as.numeric(as.character(countyfips)))

stations_counties <-
  stations %>%
  group_by(state, county, countyfips) %>%
  summarise(Local_TV_News_Count = n(),
            Sinclair_Count = sum(sinclair) + sum(sinclair2017),
            Sinclair_Share = Sinclair_Count / Local_TV_News_Count)

load("data/mobility_reports.RData")

mobility_reports <-
  mobility_reports %>%
  left_join(stations_counties, by = c("state" = "state", "fips" = "countyfips"))

mobility_reports <-
  mobility_reports %>%
  mutate(Sinclair_Count = ifelse(is.na(Sinclair_Count), 0, Sinclair_Count),
         Sinclair_Share = ifelse(is.na(Sinclair_Share), 0, Sinclair_Share),
         Sinclair_Bin = ifelse(Sinclair_Count > 0, 1, 0),
         Sinclair_Share_std = (Sinclair_Share - mean(Sinclair_Share, 
                                                     na.rm = TRUE)) /
           sd(Sinclair_Share))

mobility_reports_2 <-
  mobility_reports %>%
  filter(!is.na(cases_std),
         !is.na(deaths_std),
         !is.na(days_since_order_2_std),
         !is.na(total_population_std),
         !is.na(POP_SQMI_std),
         !is.na(median_age_std),
         !is.na(pct_non_white_std),
         !is.na(pct_college_deg_std),
         !is.na(pct_below_pov_lvl_std),
         !is.na(pct_above_2_pov_lvl_std),
         !is.na(pct_with_comp_broadband_std),
         !is.na(pct_speak_only_english_std),
         !is.na(state_capital),
         !is.na(dem_pct_std),
         !is.na(PCT_LACCESS_POP15_std),
         !is.na(Sinclair_Share_std))

cbgps_fit <- npCBPS(
  Sinclair_Share_std ~
    cases_std + # cases on Mar 29
    deaths_std + # deaths on Mar 29
    before_mar_29 +
    days_since_order_2_std + # stay at home order by Mar 29
    total_population_std + # county pop.
    POP_SQMI_std + # county pop. density
    median_age_std +
    pct_non_white_std + # share of pop not white
    pct_college_deg_std + # share of pop with any college deg
    pct_below_pov_lvl_std +  # share of pop below pov line
    pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
    pct_with_comp_broadband_std + # share with computer and broadband int
    pct_speak_only_english_std + # share that only speak english at home
    PCT_LACCESS_POP15_std + # share with low access to food
    state_capital + # state capital?
    dem_pct_std, # partisan lean
  data = mobility_reports_2, corprior = 5e-10
)
balance(cbgps_fit)

# mean correlation for balanced covariates
mean(balance(cbgps_fit)$balanced)

# store balanced/unweighted correlations in df
balance_df <- as.data.frame(
  rbind(balance(cbgps_fit)$balanced, balance(cbgps_fit)$unweighted)
)
balance_df$group <- c(rep("Balanced", nrow(balance_df) / 2), 
                      rep("Unweighted", nrow(balance_df) / 2))
balance_df$group <- factor(balance_df$group, 
                           levels = c("Unweighted", "Balanced"))

# test to see if balancing reduces correlations with treatment
wilcox.test(balance_df$`Pearson Correlation`[which(balance_df$group == 
                                                     "Balanced")], 
            balance_df$`Pearson Correlation`[which(balance_df$group ==
                                                     "Unweighted")], 
            alternative = "less")

# plot figure 3
ggplot(balance_df, aes(x = group, y = `Pearson Correlation`)) +
  geom_boxplot() +
  scale_x_discrete("Group") +
  fischeR::theme_saf_no_font()

mobility_reports_2$weights <- cbgps_fit$weights

m1_residential_CBGPS <- 
  felm(residential ~ 
         Sinclair_Share_std +
         cases_std + # cases on Mar 29
         deaths_std + # deaths on Mar 29
         before_mar_29 +
         days_since_order_2_std + # stay at home order by Mar 29
         total_population_std + # county pop.
         POP_SQMI_std + # county pop. density
         median_age_std +
         pct_non_white_std + # share of pop not white
         pct_college_deg_std + # share of pop with any college deg
         pct_below_pov_lvl_std +  # share of pop below pov line
         pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
         pct_with_comp_broadband_std + # share with computer and broadband int
         pct_speak_only_english_std + # share that only speak english at home
         PCT_LACCESS_POP15_std + # share with low access to food
         state_capital + # state capital?
         dem_pct_std # partisan lean
       | 0 | 0 | state, 
       data = mobility_reports_2, weights = mobility_reports_2$weights)
summary(m1_residential_CBGPS)
confint(m1_residential_CBGPS)

m1_recreation_CBGPS <- 
  felm(retail_recreation ~ 
         Sinclair_Share_std +
         cases_std + # cases on Mar 29
         deaths_std + # deaths on Mar 29
         before_mar_29 +
         days_since_order_2_std + # stay at home order by Mar 29
         total_population_std + # county pop.
         POP_SQMI_std + # county pop. density
         median_age_std +
         pct_non_white_std + # share of pop not white
         pct_college_deg_std + # share of pop with any college deg
         pct_below_pov_lvl_std +  # share of pop below pov line
         pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
         pct_with_comp_broadband_std + # share with computer and broadband int
         pct_speak_only_english_std + # share that only speak english at home
         PCT_LACCESS_POP15_std + # share with low access to food
         state_capital + # state capital?
         dem_pct_std # partisan lean
       | 0 | 0 | state, 
       data = mobility_reports_2, weights = mobility_reports_2$weights)
summary(m1_recreation_CBGPS)
confint(m1_recreation_CBGPS)