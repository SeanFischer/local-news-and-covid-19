# load libraries
library(dplyr)
library(lfe)
library(CBPS)
library(ggplot2)
library(patchwork)
library(lubridate)

# load mobility data with covariates
load("data/mobility_reports_4_30.RData")

# create variable for day 1-76 of window
date_df <-
  data.frame(
    date = lubridate::mdy("2-15-2020"):lubridate::mdy("4-30-2020"),
    day_int = 1:76
  )
date_df$day_int_std <- (date_df$day_int - mean(date_df$day_int)) /
  sd(date_df$day_int)
date_df$date <- lubridate::as_date(date_df$date)

# filter out buggy cases
google_mobility_2 <-
  google_mobility_2 %>%
  distinct() %>%
  group_by(fips) %>%
  filter(n() <= 76) %>%
  mutate(days_in_period = case_when(date <= mdy("3-11-2020") ~ 
                                      date - mdy("2-14-2020"),
                                    after_order == 0 & date > mdy("3-11-2020") ~
                                      date - mdy("3-11-2020"),
                                    after_order == 1 ~
                                      date - order_date)) %>%
  ungroup() %>%
  left_join(date_df, by = c("date" = "date"))

# function to generate predictions from clustered errors
# https://stackoverflow.com/questions/3790116/using-clustered-covariance-matrix-in-predict-lm
predict.rob <- function(x, clcov, newdata){
  if(missing(newdata)){ newdata <- x$model }
  tt <- terms(x)
  Terms <- delete.response(tt)
  m.mat <- model.matrix(Terms, data = newdata)
  m.coef <- x$coef
  fit <- as.vector(m.mat %*% x$coef)
  se.fit <- sqrt(diag(m.mat%*%clcov%*%t(m.mat)))
  return(list(fit=fit,se.fit=se.fit))
}

# first DiD model with covariate adjustment
m_residential_poly <- lm(residential_std ~ 
                      paper_count_std * time_period * poly(day_int, 2) +
                      cases_std_all +
                      deaths_std_all + 
                      total_population_std + # county pop.
                      POP_SQMI_std + # county pop. density
                      median_age_std  + # county median age
                      pct_non_white_std  + # share of pop not white
                      pct_college_deg_std + # share of pop with any college deg
                      pct_below_pov_lvl_std +  # share of pop below pov line
                      pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
                      pct_with_comp_broadband_std + # share with computer and broadband int
                      pct_speak_only_english_std + # share that only speak english at home
                      PCT_LACCESS_POP15_std + # share with low access to food
                      state_capital + # state capital?
                      dem_pct_std,
                    data = google_mobility_2 %>% filter(!is.na(days_in_period)))
# get clustered standard errors
vcov_state_county_poly <- multiwayvcov::cluster.vcov(m_residential_poly,
                                                ~ state_abrv + fips)
# summary output using clustered standard errors
lmtest::coeftest(m_residential_poly, vcov = vcov_state_county_poly)
confint(lmtest::coeftest(m_residential_poly, vcov = vcov_state_county_poly))
# generate new data for predictions
new_data_res_poly <- expand.grid(
  day_int = 1:76,
  cases_std_all = 0,
  deaths_std_all = 0,
  total_population_std = 0,
  POP_SQMI_std = 0,
  median_age_std = 0,
  pct_non_white_std = 0,
  pct_college_deg_std = 0,
  pct_below_pov_lvl_std = 0,
  pct_above_2_pov_lvl_std = 0,
  pct_with_comp_broadband_std = 0,
  pct_speak_only_english_std = 0,
  PCT_LACCESS_POP15_std = 0,
  state_capital = 0,
  dem_pct_std = 0)

new_data_res_poly <-
  new_data_res_poly %>%
  mutate(time_period = case_when(row_number() <= 26 ~ "Time 1",
                                 row_number() > 47 ~ "Time 3",
                                 TRUE ~ "Time 2"))

new_data_res_poly <-
  new_data_res_poly %>%
  bind_rows(new_data_res_poly) %>%
  bind_rows(new_data_res_poly) %>%
  bind_rows(new_data_res_poly) %>%
  bind_rows(new_data_res_poly) %>%
  bind_rows(new_data_res_poly) %>%
  bind_rows(new_data_res_poly) %>%
  bind_rows(new_data_res_poly) %>%
  bind_rows(new_data_res_poly) %>%
  mutate(paper_count_std = c(rep(-0.666, 76),
                             rep(0.7965, 76),
                             rep(2.259, 76),
                             rep(3.7215, 76),
                             rep(5.184, 76),
                             rep(6.8963, 76),
                             rep(8.2755, 76),
                             rep(9.6548, 76),
                             rep(11.0341, 76)),
         paper_count_label = case_when(paper_count_std == -0.666 ~ "0 Papers",
                                       paper_count_std == 0.7965 ~ "5 Papers",
                                       paper_count_std == 2.259 ~ "10 Papers",
                                       paper_count_std == 3.7215 ~ "15 Papers",
                                       paper_count_std == 5.184 ~ "20 Papers",
                                       paper_count_std == 6.8963 ~ "25 Papers",
                                       paper_count_std == 8.2755 ~ "30 Papers",
                                       paper_count_std == 9.6548 ~ "35 Papers",
                                       paper_count_std == 11.0341 ~ "40 Papers"),
         paper_count_label = factor(paper_count_label,
                                    levels = c("0 Papers", "5 Papers",
                                               "10 Papers", "15 Papers",
                                               "20 Papers", "25 Papers",
                                               "30 Papers", "35 Papers",
                                               "40 Papers")))

# predict outcomes and get estimates
new_data_res_poly$outcome <- predict.rob(m_residential_poly, 
                                    vcov_state_county_poly, 
                                    new_data_res_poly)$fit
# predict outcomes and get se for estimates
new_data_res_poly$se_outcome <- predict.rob(m_residential_poly, 
                                       vcov_state_county_poly, 
                                       new_data_res_poly)$se.fit
# continuous scale time period labels
new_data_res_poly$time_period_2 <- ifelse(new_data_res_poly$time_period == "Time 1", "Before March 12", 
                                     ifelse(new_data_res_poly$time_period == "Time 2", 
                                            "March 12 - Statewide Order", "After Statewide Order"))
new_data_res_poly$time_period_2 <- factor(new_data_res_poly$time_period_2,
                                     levels = c("Before March 12",
                                                "March 12 - Statewide Order",
                                                "After Statewide Order"))

# plot predictions
ggplot(new_data_res_poly %>% 
         mutate(
           lwr = outcome - 1.96 * se_outcome,
           upr = outcome + 1.96 * se_outcome), 
       aes(x = day_int, y = outcome)) + 
  geom_hline(yintercept = 0, lty = 2) +
  geom_hline(yintercept = 1, lty = 2) +
  geom_hline(yintercept = 2, lty = 2) +
  geom_vline(xintercept = 26, lty = 3, color = "grey50") +
  geom_vline(xintercept = 47, lty = 3, color = "grey50") +
  # geom_point() +
  geom_line() +
  geom_ribbon(aes(ymin = lwr, ymax = upr), alpha = 0.2) +
  facet_wrap(~ paper_count_label) +
  xlab("Day") + 
  ylab("Predicted Standard-Deviation Change in Mobility") +
  # scale_x_continuous(breaks = c(1,
  #                               2.2590,  5.1840,
  #                               8.2755, 11.0341),
  #                    labels = c(0, 10, 20, 30, 40)) +
  # scale_color_brewer(name = "Time Period", palette = "Paired") +
  guides(fill = FALSE) +
  fischeR::theme_saf_no_font() + 
  theme(panel.border = element_blank(), # remove borders
        # add x-axis line
        axis.line.x = element_line(),
        axis.line.y = element_line(),
        strip.text = element_text(colour = "black",
                                  family = "Helvetica Neue"),
        strip.background = element_rect(colour = "white",
                                        fill = "white"))

# first DiD model with covariate adjustment
m_recreation_poly <- lm(recreation_std ~ 
                           paper_count_std * time_period * poly(day_int, 2) +
                           cases_std_all +
                           deaths_std_all + 
                           total_population_std + # county pop.
                           POP_SQMI_std + # county pop. density
                           median_age_std  + # county median age
                           pct_non_white_std  + # share of pop not white
                           pct_college_deg_std + # share of pop with any college deg
                           pct_below_pov_lvl_std +  # share of pop below pov line
                           pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
                           pct_with_comp_broadband_std + # share with computer and broadband int
                           pct_speak_only_english_std + # share that only speak english at home
                           PCT_LACCESS_POP15_std + # share with low access to food
                           state_capital + # state capital?
                           dem_pct_std,
                         data = google_mobility_2 %>% filter(!is.na(days_in_period)))
# get clustered standard errors
vcov_state_county_rec_poly <- multiwayvcov::cluster.vcov(m_recreation_poly,
                                                     ~ state_abrv + fips)
# summary output using clustered standard errors
lmtest::coeftest(m_recreation_poly, vcov = vcov_state_county_rec_poly)
confint(lmtest::coeftest(m_recreation_poly, vcov = vcov_state_county_rec_poly))
# generate new data for predictions
new_data_rec_poly <- expand.grid(
  day_int = 1:76,
  cases_std_all = 0,
  deaths_std_all = 0,
  total_population_std = 0,
  POP_SQMI_std = 0,
  median_age_std = 0,
  pct_non_white_std = 0,
  pct_college_deg_std = 0,
  pct_below_pov_lvl_std = 0,
  pct_above_2_pov_lvl_std = 0,
  pct_with_comp_broadband_std = 0,
  pct_speak_only_english_std = 0,
  PCT_LACCESS_POP15_std = 0,
  state_capital = 0,
  dem_pct_std = 0)

new_data_rec_poly <-
  new_data_rec_poly %>%
  mutate(time_period = case_when(row_number() <= 26 ~ "Time 1",
                                 row_number() > 47 ~ "Time 3",
                                 TRUE ~ "Time 2"))

new_data_rec_poly <-
  new_data_rec_poly %>%
  bind_rows(new_data_rec_poly) %>%
  bind_rows(new_data_rec_poly) %>%
  bind_rows(new_data_rec_poly) %>%
  bind_rows(new_data_rec_poly) %>%
  bind_rows(new_data_rec_poly) %>%
  bind_rows(new_data_rec_poly) %>%
  bind_rows(new_data_rec_poly) %>%
  bind_rows(new_data_rec_poly) %>%
  mutate(paper_count_std = c(rep(-0.666, 76),
                             rep(0.7965, 76),
                             rep(2.259, 76),
                             rep(3.7215, 76),
                             rep(5.184, 76),
                             rep(6.8963, 76),
                             rep(8.2755, 76),
                             rep(9.6548, 76),
                             rep(11.0341, 76)),
         paper_count_label = case_when(paper_count_std == -0.666 ~ "0 Papers",
                                       paper_count_std == 0.7965 ~ "5 Papers",
                                       paper_count_std == 2.259 ~ "10 Papers",
                                       paper_count_std == 3.7215 ~ "15 Papers",
                                       paper_count_std == 5.184 ~ "20 Papers",
                                       paper_count_std == 6.8963 ~ "25 Papers",
                                       paper_count_std == 8.2755 ~ "30 Papers",
                                       paper_count_std == 9.6548 ~ "35 Papers",
                                       paper_count_std == 11.0341 ~ "40 Papers"),
         paper_count_label = factor(paper_count_label,
                                    levels = c("0 Papers", "5 Papers",
                                               "10 Papers", "15 Papers",
                                               "20 Papers", "25 Papers",
                                               "30 Papers", "35 Papers",
                                               "40 Papers")))

# predict outcomes and get estimates
new_data_rec_poly$outcome <- predict.rob(m_recreation_poly, 
                                         vcov_state_county_rec_poly, 
                                         new_data_rec_poly)$fit
# predict outcomes and get se for estimates
new_data_rec_poly$se_outcome <- predict.rob(m_recreation_poly, 
                                            vcov_state_county_rec_poly, 
                                            new_data_rec_poly)$se.fit
# continuous scale time period labels
new_data_rec_poly$time_period_2 <- ifelse(new_data_rec_poly$time_period == "Time 1", "Before March 12", 
                                          ifelse(new_data_rec_poly$time_period == "Time 2", 
                                                 "March 12 - Statewide Order", "After Statewide Order"))
new_data_rec_poly$time_period_2 <- factor(new_data_rec_poly$time_period_2,
                                          levels = c("Before March 12",
                                                     "March 12 - Statewide Order",
                                                     "After Statewide Order"))

# plot predictions
ggplot(new_data_rec_poly %>% 
         mutate(
           lwr = outcome - 1.96 * se_outcome,
           upr = outcome + 1.96 * se_outcome), 
       aes(x = day_int, y = outcome)) + 
  geom_hline(yintercept = 0, lty = 2) +
  geom_hline(yintercept = -1, lty = 2) +
  geom_hline(yintercept = -2, lty = 2) +
  geom_vline(xintercept = 26, lty = 3, color = "grey50") +
  geom_vline(xintercept = 47, lty = 3, color = "grey50") +
  # geom_point() +
  geom_line() +
  geom_ribbon(aes(ymin = lwr, ymax = upr), alpha = 0.2) +
  facet_wrap(~ paper_count_label) +
  xlab("Day") + 
  ylab("Predicted Standard-Deviation Change in Mobility") +
  # scale_x_continuous(breaks = c(1,
  #                               2.2590,  5.1840,
  #                               8.2755, 11.0341),
  #                    labels = c(0, 10, 20, 30, 40)) +
  # scale_color_brewer(name = "Time Period", palette = "Paired") +
  guides(fill = FALSE) +
  fischeR::theme_saf_no_font() + 
  theme(panel.border = element_blank(), # remove borders
        # add x-axis line
        axis.line.x = element_line(),
        axis.line.y = element_line(),
        strip.text = element_text(colour = "black",
                                  family = "Helvetica Neue"),
        strip.background = element_rect(colour = "white",
                                        fill = "white"))

mobility_reports_2 <-
  google_mobility_2 %>%
  dplyr::filter(!is.na(cases_std_all),
                !is.na(deaths_std_all),
                !is.na(order_date_std),
                !is.na(after_order),
                !is.na(total_population_std),
                !is.na(POP_SQMI_std),
                !is.na(median_age_std),
                !is.na(pct_non_white_std),
                !is.na(pct_college_deg_std),
                !is.na(pct_below_pov_lvl_std),
                !is.na(pct_above_2_pov_lvl_std),
                !is.na(pct_with_comp_broadband_std),
                !is.na(pct_speak_only_english_std),
                !is.na(state_capital),
                !is.na(dem_pct_std),
                !is.na(PCT_LACCESS_POP15_std),
                !is.na(paper_count_std))

load("data/counties_weights_main.RData")

# merge weights into daily data
mobility_reports_2 <-
  mobility_reports_2 %>%
  inner_join(counties)

# first DiD model with covariate adjustment
m_residential_poly_CBGPS <- lm(residential_std ~ 
                           paper_count_std * time_period * poly(days_in_period, 2) +
                           cases_std_all +
                           deaths_std_all + 
                           total_population_std + # county pop.
                           POP_SQMI_std + # county pop. density
                           median_age_std  + # county median age
                           pct_non_white_std  + # share of pop not white
                           pct_college_deg_std + # share of pop with any college deg
                           pct_below_pov_lvl_std +  # share of pop below pov line
                           pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
                           pct_with_comp_broadband_std + # share with computer and broadband int
                           pct_speak_only_english_std + # share that only speak english at home
                           PCT_LACCESS_POP15_std + # share with low access to food
                           state_capital + # state capital?
                           dem_pct_std,
                         data = mobility_reports_2 %>% 
                           filter(!is.na(days_in_period)),
                         weights = weights)
# get clustered standard errors
vcov_state_county_poly_CBGPS <- multiwayvcov::cluster.vcov(m_residential_poly_CBGPS,
                                                     ~ state_abrv + fips)
# summary output using clustered standard errors
lmtest::coeftest(m_residential_poly_CBGPS, vcov = vcov_state_county_poly_CBGPS)
confint(lmtest::coeftest(m_residential_poly_CBGPS, vcov = vcov_state_county_poly_CBGPS))
# generate new data for predictions
new_data_res_poly_CBGPS <- expand.grid(
  time_period = c("Time 1", "Time 2", "Time 3"),
  days_in_period = 1:26,
  cases_std_all = 0,
  deaths_std_all = 0,
  total_population_std = 0,
  POP_SQMI_std = 0,
  median_age_std = 0,
  pct_non_white_std = 0,
  pct_college_deg_std = 0,
  pct_below_pov_lvl_std = 0,
  pct_above_2_pov_lvl_std = 0,
  pct_with_comp_broadband_std = 0,
  pct_speak_only_english_std = 0,
  PCT_LACCESS_POP15_std = 0,
  state_capital = 0,
  dem_pct_std = 0)

new_data_res_poly_CBGPS <-
  new_data_res_poly_CBGPS %>%
  bind_rows(new_data_res_poly_CBGPS) %>%
  bind_rows(new_data_res_poly_CBGPS) %>%
  bind_rows(new_data_res_poly_CBGPS) %>%
  bind_rows(new_data_res_poly_CBGPS) %>%
  bind_rows(new_data_res_poly_CBGPS) %>%
  bind_rows(new_data_res_poly_CBGPS) %>%
  bind_rows(new_data_res_poly_CBGPS) %>%
  bind_rows(new_data_res_poly_CBGPS) %>%
  mutate(paper_count_std = c(rep(-0.666, 78),
                             rep(0.7965, 78),
                             rep(2.259, 78),
                             rep(3.7215, 78),
                             rep(5.184, 78),
                             rep(6.8963, 78),
                             rep(8.2755, 78),
                             rep(9.6548, 78),
                             rep(11.0341, 78)),
         paper_count_label = case_when(paper_count_std == -0.666 ~ "0 Papers",
                                       paper_count_std == 0.7965 ~ "5 Papers",
                                       paper_count_std == 2.259 ~ "10 Papers",
                                       paper_count_std == 3.7215 ~ "15 Papers",
                                       paper_count_std == 5.184 ~ "20 Papers",
                                       paper_count_std == 6.8963 ~ "25 Papers",
                                       paper_count_std == 8.2755 ~ "30 Papers",
                                       paper_count_std == 9.6548 ~ "35 Papers",
                                       paper_count_std == 11.0341 ~ "40 Papers"),
         paper_count_label = factor(paper_count_label,
                                    levels = c("0 Papers", "5 Papers",
                                               "10 Papers", "15 Papers",
                                               "20 Papers", "25 Papers",
                                               "30 Papers", "35 Papers",
                                               "40 Papers"))) %>%
  group_by(paper_count_std) %>%
  arrange(time_period, days_in_period) %>%
  mutate(day = 1:78) %>%
  ungroup()

# predict outcomes and get estimates
new_data_res_poly_CBGPS$outcome <- predict.rob(m_residential_poly_CBGPS, 
                                         vcov_state_county_poly_CBGPS, 
                                         new_data_res_poly_CBGPS)$fit
# predict outcomes and get se for estimates
new_data_res_poly_CBGPS$se_outcome <- predict.rob(m_residential_poly_CBGPS, 
                                            vcov_state_county_poly_CBGPS, 
                                            new_data_res_poly)$se.fit
# continuous scale time period labels
new_data_res_poly_CBGPS$time_period_2 <- ifelse(new_data_res_poly_CBGPS$time_period == "Time 1", "Before March 12", 
                                          ifelse(new_data_res_poly_CBGPS$time_period == "Time 2", 
                                                 "March 12 - Statewide Order", "After Statewide Order"))
new_data_res_poly$time_period_2 <- factor(new_data_res_poly_CBGPS$time_period_2,
                                          levels = c("Before March 12",
                                                     "March 12 - Statewide Order",
                                                     "After Statewide Order"))

# plot predictions
ggplot(new_data_res_poly_CBGPS %>% 
         mutate(
           lwr = outcome - 1.96 * se_outcome,
           upr = outcome + 1.96 * se_outcome), 
       aes(x = day, y = outcome)) + 
  geom_hline(yintercept = 0, lty = 2) +
  geom_hline(yintercept = 1, lty = 2) +
  geom_hline(yintercept = 2, lty = 2) +
  geom_vline(xintercept = 26, lty = 3, color = "grey50") +
  geom_vline(xintercept = 52, lty = 3, color = "grey50") +
  # geom_point() +
  geom_line() +
  geom_ribbon(aes(ymin = lwr, ymax = upr), alpha = 0.2) +
  facet_wrap(~ paper_count_label) +
  xlab("Day") + 
  ylab("Predicted Standard-Deviation Change in Mobility") +
  # scale_x_continuous(breaks = c(1,
  #                               2.2590,  5.1840,
  #                               8.2755, 11.0341),
  #                    labels = c(0, 10, 20, 30, 40)) +
  # scale_color_brewer(name = "Time Period", palette = "Paired") +
  guides(fill = FALSE) +
  fischeR::theme_saf_no_font() + 
  theme(panel.border = element_blank(), # remove borders
        # add x-axis line
        axis.line.x = element_line(),
        axis.line.y = element_line(),
        strip.text = element_text(colour = "black",
                                  family = "Helvetica Neue"),
        strip.background = element_rect(colour = "white",
                                        fill = "white"))

# first DiD model with covariate adjustment
m_recreation_poly_CBGPS <- lm(recreation_std ~ 
                          paper_count_std * time_period * poly(days_in_period, 2) +
                          cases_std_all +
                          deaths_std_all + 
                          total_population_std + # county pop.
                          POP_SQMI_std + # county pop. density
                          median_age_std  + # county median age
                          pct_non_white_std  + # share of pop not white
                          pct_college_deg_std + # share of pop with any college deg
                          pct_below_pov_lvl_std +  # share of pop below pov line
                          pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
                          pct_with_comp_broadband_std + # share with computer and broadband int
                          pct_speak_only_english_std + # share that only speak english at home
                          PCT_LACCESS_POP15_std + # share with low access to food
                          state_capital + # state capital?
                          dem_pct_std,
                        data = mobility_reports_2 %>% 
                          filter(!is.na(days_in_period)),
                        weights = weights)
# get clustered standard errors
vcov_state_county_rec_poly_CBGPS <- multiwayvcov::cluster.vcov(m_recreation_poly_CBGPS,
                                                         ~ state_abrv + fips)
# summary output using clustered standard errors
lmtest::coeftest(m_recreation_poly_CBGPS, vcov = vcov_state_county_rec_poly_CBGPS)
confint(lmtest::coeftest(m_recreation_poly_CBGPS, vcov = vcov_state_county_rec_poly_CBGPS))
# generate new data for predictions
new_data_rec_poly_CBGPS <- expand.grid(
  time_period = c("Time 1", "Time 2", "Time 3"),
  days_in_period = 1:26,
  cases_std_all = 0,
  deaths_std_all = 0,
  total_population_std = 0,
  POP_SQMI_std = 0,
  median_age_std = 0,
  pct_non_white_std = 0,
  pct_college_deg_std = 0,
  pct_below_pov_lvl_std = 0,
  pct_above_2_pov_lvl_std = 0,
  pct_with_comp_broadband_std = 0,
  pct_speak_only_english_std = 0,
  PCT_LACCESS_POP15_std = 0,
  state_capital = 0,
  dem_pct_std = 0)

new_data_rec_poly_CBGPS <-
  new_data_rec_poly_CBGPS %>%
  bind_rows(new_data_rec_poly_CBGPS) %>%
  bind_rows(new_data_rec_poly_CBGPS) %>%
  bind_rows(new_data_rec_poly_CBGPS) %>%
  bind_rows(new_data_rec_poly_CBGPS) %>%
  bind_rows(new_data_rec_poly_CBGPS) %>%
  bind_rows(new_data_rec_poly_CBGPS) %>%
  bind_rows(new_data_rec_poly_CBGPS) %>%
  bind_rows(new_data_rec_poly_CBGPS) %>%
  mutate(paper_count_std = c(rep(-0.666, 78),
                             rep(0.7965, 78),
                             rep(2.259, 78),
                             rep(3.7215, 78),
                             rep(5.184, 78),
                             rep(6.8963, 78),
                             rep(8.2755, 78),
                             rep(9.6548, 78),
                             rep(11.0341, 78)),
         paper_count_label = case_when(paper_count_std == -0.666 ~ "0 Papers",
                                       paper_count_std == 0.7965 ~ "5 Papers",
                                       paper_count_std == 2.259 ~ "10 Papers",
                                       paper_count_std == 3.7215 ~ "15 Papers",
                                       paper_count_std == 5.184 ~ "20 Papers",
                                       paper_count_std == 6.8963 ~ "25 Papers",
                                       paper_count_std == 8.2755 ~ "30 Papers",
                                       paper_count_std == 9.6548 ~ "35 Papers",
                                       paper_count_std == 11.0341 ~ "40 Papers"),
         paper_count_label = factor(paper_count_label,
                                    levels = c("0 Papers", "5 Papers",
                                               "10 Papers", "15 Papers",
                                               "20 Papers", "25 Papers",
                                               "30 Papers", "35 Papers",
                                               "40 Papers"))) %>%
  group_by(paper_count_std) %>%
  arrange(time_period, days_in_period) %>%
  mutate(day = 1:78) %>%
  ungroup()

# predict outcomes and get estimates
new_data_rec_poly_CBGPS$outcome <- predict.rob(m_recreation_poly_CBGPS, 
                                         vcov_state_county_rec_poly_CBGPS, 
                                         new_data_rec_poly_CBGPS)$fit
# predict outcomes and get se for estimates
new_data_rec_poly_CBGPS$se_outcome <- predict.rob(m_recreation_poly_CBGPS, 
                                            vcov_state_county_rec_poly_CBGPS, 
                                            new_data_rec_poly_CBGPS)$se.fit
# continuous scale time period labels
new_data_rec_poly_CBGPS$time_period_2 <- ifelse(new_data_rec_poly_CBGPS$time_period == "Time 1", "Before March 12", 
                                          ifelse(new_data_rec_poly_CBGPS$time_period == "Time 2", 
                                                 "March 12 - Statewide Order", "After Statewide Order"))
new_data_rec_poly_CBGPS$time_period_2 <- factor(new_data_rec_poly_CBGPS$time_period_2,
                                          levels = c("Before March 12",
                                                     "March 12 - Statewide Order",
                                                     "After Statewide Order"))

# plot predictions
ggplot(new_data_rec_poly_CBGPS %>% 
         mutate(
           lwr = outcome - 1.96 * se_outcome,
           upr = outcome + 1.96 * se_outcome), 
       aes(x = day, y = outcome)) + 
  geom_hline(yintercept = 0, lty = 2) +
  geom_hline(yintercept = -1, lty = 2) +
  geom_hline(yintercept = -2, lty = 2) +
  geom_vline(xintercept = 26, lty = 3, color = "grey50") +
  geom_vline(xintercept = 52, lty = 3, color = "grey50") +
  # geom_point() +
  geom_line() +
  geom_ribbon(aes(ymin = lwr, ymax = upr), alpha = 0.2) +
  facet_wrap(~ paper_count_label) +
  xlab("Day") + 
  ylab("Predicted Standard-Deviation Change in Mobility") +
  # scale_x_continuous(breaks = c(1,
  #                               2.2590,  5.1840,
  #                               8.2755, 11.0341),
  #                    labels = c(0, 10, 20, 30, 40)) +
  # scale_color_brewer(name = "Time Period", palette = "Paired") +
  guides(fill = FALSE) +
  fischeR::theme_saf_no_font() + 
  theme(panel.border = element_blank(), # remove borders
        # add x-axis line
        axis.line.x = element_line(),
        axis.line.y = element_line(),
        strip.text = element_text(colour = "black",
                                  family = "Helvetica Neue"),
        strip.background = element_rect(colour = "white",
                                        fill = "white"))
