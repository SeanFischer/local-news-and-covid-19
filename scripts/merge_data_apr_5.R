library(dplyr)
library(stringr)
library(tidycensus)
library(tidyr)

mobility_reports_4_5 <- read.csv("data/mobility_reports_4_05.csv",
                             stringsAsFactors = FALSE)

mobility_reports_4_5 <-
  mobility_reports_4_5 %>%
  filter(!is.na(state)) %>%
  mutate(county = str_remove(county, " County"),
         county = str_remove(county, " Parish"),
         date = as_date(as.character(date)))

fips_codes <-
  fips_codes %>%
  mutate(fips = paste0(state_code, county_code),
         county = str_remove(county, " County"),
         county = str_remove(county, " Parish"),
         fips = as.numeric(fips))

unc_newspapers <- read.csv("data/unc_newspaper_data.csv",
                           stringsAsFactors = FALSE)
unc_newspapers$total_circulation <- as.numeric(unc_newspapers$total_circulation)
unc_newspapers$days_published <- as.numeric(unc_newspapers$days_published)
unc_newspapers_2 <-
  unc_newspapers %>%
  group_by(state, county) %>%
  summarise(paper_count = n(),
            mean_days_pub = weighted.mean(days_published, 
                                          w = total_circulation,
                                          na.rm = TRUE),
            mean_circulation = mean(total_circulation, na.rm = TRUE))

state_orders <- read.csv("data/state_wide_covid_orders.csv",
                         stringsAsFactors = FALSE)
# county_orders <- read.csv("data/county_wide_covid_orders.csv",
# stringsAsFactors = FALSE)

election_results_2016 <- read.csv("data/countiesElecResults.csv",
                                  stringsAsFactors = FALSE)

food_access <- read.csv("data/food_access_county.csv",
                        stringsAsFactors = FALSE)

pop_density <- read.csv("data/county_pop_density.csv",
                        stringsAsFactors = FALSE)
pop_density <-
  pop_density %>%
  dplyr::select(FIPS, POP_SQMI, SQMI)

source("scripts/get_covid_cases_nyt.R")

census_key <- "76d56f960e229f0b05a2087895776512a3b232a"
census_dat <-
  get_acs(
    geography = "county",
    variables = c(
      total_households = "B09019_001", # total households
      total_population = "B01003_001", # total population
      total_under_18 = "B09001_001", # population under 18
      white_population = "B01001A_001", # white population
      black_population = "B01001B_001", # black population
      am_indian_population = "B01001C_001", # am indian population
      asian_population = "B01001D_001", # asian population
      nat_hawaiian_population = "B01001E_001", # nat hawiian population
      other_race_population = "B01001F_001", # other race population
      two_races_population = "B01001G_001", # two races population
      non_hisp_population = "B01001H_001", # non-hispanic white population
      hispanic_population = "B01001I_001", # hispanic population
      median_age = "B01002_001", # median age
      pop_below_pov_lvl_1 = "C17002_002", # total below 0.5 of poverty level
      pop_below_pov_lvl_2 = "C17002_003", # total between 0.5-0.99 of pov level
      pop_above_2_pov_lvl = "C17002_008", # total above 2 times poverty level
      total_high_school = "B15003_017", # total high school as highest
      total_ged = "B15003_018", # total receiving GED as highest
      total_some_college_less_than_1_year = "B15003_019",
      total_some_college_more_than_1_year = "B15003_020",
      total_assoc_deg = "B15003_021",
      total_bach_deg = "B15003_022",
      total_master_deg = "B15003_023",
      total_prof_deg = "B15003_024",
      total_doctorate_deg = "B15003_025",
      total_with_comp_broadband = "B28003_004", # households w/ comp and broadband
      total_with_insurance = "B27001_001",
      total_speak_only_english = "C16001_002" # total speaking only eng @ home
    )
  )
census_dat <-
  census_dat %>%
  dplyr::select(-moe) %>%
  spread(variable, estimate) %>%
  mutate(GEOID = gsub("(^|[^0-9])0+", "\\1", GEOID, perl = TRUE),
         GEOID = as.numeric(GEOID),
         log_population = log(total_population), # log of population
         voting_pop = total_population - total_under_18,
         hisp_white_population = white_population - non_hisp_population,
         hisp_n_white_population = hispanic_population - hisp_white_population,
         total_high_school_ged = (total_high_school + total_ged),
         total_some_college = (total_some_college_less_than_1_year +
                                 total_some_college_more_than_1_year),
         am_indian_pct = am_indian_population / total_population,
         asian_pct = asian_population / total_population,
         black_pct = black_population / total_population,
         hispanic_pct = hispanic_population / total_population,
         nat_hawaiian_pct = nat_hawaiian_population / total_population,
         white_non_hisp_pct = non_hisp_population / total_population,
         other_race_pct = other_race_population / total_population,
         two_races_pct = two_races_population / total_population,
         white_pct = white_population / total_population,
         hisp_white_pct = hisp_white_population / total_population,
         hisp_non_white_pct = hisp_n_white_population / total_population,
         pct_non_white = 1 - white_pct,
         pct_below_pov_lvl_1 = pop_below_pov_lvl_1 / total_population,
         pct_below_pov_lvl_2 = pop_below_pov_lvl_2 / total_population,
         pct_below_pov_lvl = pct_below_pov_lvl_1 + pct_below_pov_lvl_2,
         pct_above_2_pov_lvl = pop_above_2_pov_lvl / total_population,
         pct_high_school_ged = total_high_school_ged / total_population,
         pct_some_college = total_some_college / total_population,
         pct_assoc_deg = total_assoc_deg / total_population,
         pct_bach_deg = total_bach_deg / total_population,
         pct_doctorate_deg = total_doctorate_deg / total_population,
         pct_master_deg = total_master_deg / total_population,
         pct_prof_deg = total_prof_deg / total_population,
         pct_college_deg = (pct_assoc_deg + pct_bach_deg + pct_master_deg +
                              pct_prof_deg + pct_doctorate_deg),
         pct_with_comp_broadband = total_with_comp_broadband / total_households,
         pct_with_insurance = total_with_insurance / total_population,
         pct_speak_only_english = total_speak_only_english / total_population
  )

state_capitals_fips <- c(1101, 4013, 5119, 6067, 8031, 9003, 10001, 12073,
                         13121, 13089, 16001, 18097, 17167, 19057, 20177,
                         21073, 22033, 22121, 23011, 24003, 25025, 26065,
                         27123, 28049, 29051, 30049, 31109, 32510, 33013,
                         34021, 35049, 36001, 37183, 38015, 39049, 40109,
                         41047, 42043, 44007, 45079, 46065, 47037, 48453,
                         49035, 50023, 51087, 51041, 53067, 54039, 55025,
                         56021)

mobility_reports_4_5 <-
  mobility_reports_4_5 %>%
  left_join(fips_codes %>% dplyr::select(state, county, fips), 
            by = c("state" = "state", "county" = "county")) %>%
  left_join(covid_data_4_05 %>% dplyr::select(-fips),
            by = c("state_2" = "state", "county" = "county")) %>%
  left_join(census_dat, by = c("fips" = "GEOID")) %>%
  left_join(unc_newspapers_2, 
            by = c("state" = "state", "county" = "county")) %>%
  left_join(state_orders,
            by = c("state" = "state")) %>%
  left_join(election_results_2016 %>% dplyr::select(-county),
            by = c("fips" = "fips_code")) %>%
  left_join(food_access %>% dplyr::select(-FIPS), 
            by = c("state" = "State", "county" = "County")) %>%
  left_join(pop_density, by = c("fips" = "FIPS")) %>%
  mutate(cases = ifelse(is.na(cases), 0 , cases),
         deaths = ifelse(is.na(deaths), 0 , deaths),
         have_cases = ifelse(cases > 0, 1, 0),
         have_deaths = ifelse(deaths > 0, 1, 0),
         days_since_order_2 = days_since_order_apr_5 * before_apr_5,
         state_capital = ifelse(fips %in% state_capitals_fips, 1, 0),
         paper_count = ifelse(is.na(paper_count), 0, paper_count),
         paper_count_per_1000 = (paper_count / total_population) * 1000,
         turnout_16 = total_2016 / voting_pop,
         cases_std = (cases - mean(cases, na.rm = TRUE)) / 
           sd(cases, na.rm = TRUE),
         deaths_std = (deaths - mean(deaths, na.rm = TRUE)) / 
           sd(deaths, na.rm = TRUE),
         # days_since_first_case_std = 
         #   (days_since_first_case - mean(days_since_first_case, na.rm = TRUE)) / 
         #   sd(days_since_first_case, na.rm = TRUE),
         # days_since_first_death_2_std = 
         #   (days_since_first_death_2 - mean(days_since_first_death_2, 
         #                                    na.rm = TRUE)) / 
         #   sd(days_since_first_death_2, na.rm = TRUE),
         pct_non_white_std = (pct_non_white - mean(pct_non_white,
                                                   na.rm = TRUE)) / 
           sd(pct_non_white,
              na.rm = TRUE),
         pct_high_school_ged_std = (pct_high_school_ged - 
                                      mean(pct_high_school_ged,
                                           na.rm = TRUE)) /
           sd(pct_high_school_ged,
              na.rm = TRUE),
         pct_college_deg_std = (pct_college_deg - mean(pct_college_deg,
                                                       na.rm = TRUE)) /
           sd(pct_college_deg,
              na.rm = TRUE),
         pct_below_pov_lvl_std = (pct_below_pov_lvl - mean(pct_below_pov_lvl,
                                                           na.rm = TRUE)) /
           sd(pct_below_pov_lvl,
              na.rm = TRUE),
         pct_above_2_pov_lvl_std = (pct_above_2_pov_lvl - 
                                      mean(pct_above_2_pov_lvl,
                                           na.rm = TRUE)) /
           sd(pct_above_2_pov_lvl,
              na.rm = TRUE),
         pct_with_comp_broadband_std = (pct_with_comp_broadband - 
                                          mean(pct_with_comp_broadband,
                                               na.rm = TRUE)) /
           sd(pct_with_comp_broadband,
              na.rm = TRUE),
         pct_with_insurance_std = (pct_with_insurance - mean(pct_with_insurance,
                                                             na.rm = TRUE)) /
           sd(pct_with_insurance,
              na.rm = TRUE),
         pct_speak_only_english_std = (pct_speak_only_english -
                                         mean(pct_speak_only_english,
                                              na.rm = TRUE)) /
           sd(pct_speak_only_english,
              na.rm = TRUE),
         dem_pct_std = (dem_pct - mean(dem_pct,
                                       na.rm = TRUE)) / sd(dem_pct,
                                                           na.rm = TRUE),
         turnout_16_std = (turnout_16 - mean(turnout_16,
                                             na.rm = TRUE)) / sd(turnout_16,
                                                                 na.rm = TRUE),
         PCT_LACCESS_POP15_std = (PCT_LACCESS_POP15 - 
                                    mean(PCT_LACCESS_POP15,
                                         na.rm = TRUE)) /
           sd(PCT_LACCESS_POP15,
              na.rm = TRUE),
         POP_SQMI_std = (POP_SQMI - mean(POP_SQMI,
                                         na.rm = TRUE)) / sd(POP_SQMI,
                                                             na.rm = TRUE),
         paper_count_std = (paper_count - mean(paper_count,
                                               na.rm = TRUE)) / sd(paper_count,
                                                                   na.rm = TRUE),
         total_population_std = (total_population - mean(total_population, 
                                                         na.rm = TRUE)) /
           sd(total_population, na.rm = TRUE),
         median_age_std = (median_age - mean(median_age, na.rm = TRUE)) /
           sd(median_age, na.rm = TRUE),
         paper_count_per_1000_std = (paper_count_per_1000 - 
                                       mean(paper_count_per_1000, na.rm = TRUE)) /
           sd(paper_count_per_1000, na.rm = TRUE),
         days_since_order_2_std = (days_since_order_2 - mean(days_since_order_2, 
                                                             na.rm = TRUE)) /
           sd(days_since_order_2, na.rm = TRUE),
         date = "Apr. 5")
