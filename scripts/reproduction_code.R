# load libraries
library(dplyr)
library(lfe)
library(ggplot2)
library(patchwork)
library(CBPS)

# load daily mobility data w/ covariates
load("data/mobility_reports_4_30.RData")

# create variable for day 1-76 of window
date_df <-
  data.frame(
    date = lubridate::mdy("2-15-2020"):lubridate::mdy("4-30-2020"),
    day_int = 1:76
    )
date_df$day_int_std <- (date_df$day_int - mean(date_df$day_int)) /
  sd(date_df$day_int)
date_df$date <- lubridate::as_date(date_df$date)

# remove duplicated rows and counties that had duplicated data
google_mobility_2 <-
  google_mobility_2 %>%
  distinct() %>%
  group_by(fips) %>%
  filter(n() <= 76) %>%
  ungroup() %>%
  left_join(date_df, by = c("date" = "date"))

# residential model, only treatment
m_residential <-
  felm(residential_std ~ 
         paper_count_std
       | 0 | 0 | state_abrv + fips, 
       data = google_mobility_2)
summary(m_residential)
confint(m_residential)

# recreation model, only treatment
m_recreation <-
  felm(recreation_std ~ 
         paper_count_std
       | 0 | 0 | state_abrv + fips, 
       data = google_mobility_2)
summary(m_recreation)
confint(m_recreation)

# residential model with pandemic covariates
m0_residential <-
  felm(residential_std ~ 
         paper_count_std + 
         cases_std_all + 
         deaths_std_all + 
         day_int_std * after_order
       | 0 | 0 | state_abrv + fips, 
       data = google_mobility_2)
summary(m0_residential)
confint(m0_residential)

# recreation model with pandemic covariates
m0_recreation <-
  felm(recreation_std ~ 
         paper_count_std + 
         cases_std_all + 
         deaths_std_all + 
         day_int_std * after_order
       | 0 | 0 | state_abrv + fips, 
       data = google_mobility_2)
summary(m0_recreation)
confint(m0_recreation)

# extract model info
to_plot <- as.data.frame(summary(m0_residential)$coefficients)
to_plot$Term <- rownames(to_plot)
to_plot <-
  to_plot %>%
  filter(Term != "(Intercept)")
to_plot$Term <- factor(to_plot$Term, levels = to_plot$Term)
to_plot$sig <- ifelse(to_plot$`Pr(>|t|)` < 0.05, "Yes", "No")


# figure 1 left panel
p1 <- 
  ggplot(to_plot, aes(x = Estimate, y = Term, shape = sig)) +
  geom_point(size = 2.25) +
  geom_errorbarh(aes(xmin = (Estimate - 1.96 * `Cluster s.e.`), 
                     xmax = (Estimate + 1.96 * `Cluster s.e.`)),
                 height = 0.5) +
  geom_vline(xintercept = 0, lty = 2) +
  scale_y_discrete(limits = rev(levels(to_plot$Term)),
                   labels = rev(c("Total Papers", "Cases",
                                  "Deaths", "Time",
                                  "Order in Effect", 
                                  "Time : Order in Effect"
                   ))) +
  scale_shape_manual(values = c(1, 19)) +
  guides(shape = FALSE) +
  ylab("Term") +
  ggtitle("Residential") +
  fischeR::theme_saf_no_font() +
  theme(axis.text = element_text(size = 12),
        title = element_text(size = 14))

# extract model information
to_plot_2 <- as.data.frame(summary(m0_recreation)$coefficients)
to_plot_2$Term <- rownames(to_plot_2)
to_plot_2 <-
  to_plot_2 %>%
  filter(Term != "(Intercept)")
to_plot_2$Term <- factor(to_plot_2$Term, levels = to_plot_2$Term)
to_plot_2$sig <- ifelse(to_plot_2$`Pr(>|t|)` < 0.05, "Yes", "No")


# figure 1 right panel
p2 <-
  ggplot(to_plot_2, aes(x = Estimate, y = Term, shape = sig)) +
  geom_point(size = 2.25) +
  geom_errorbarh(aes(xmin = (Estimate - 1.96 * `Cluster s.e.`), 
                     xmax = (Estimate + 1.96 * `Cluster s.e.`)),
                 height = 0.5) +
  geom_vline(xintercept = 0, lty = 2) +
  scale_y_discrete(limits = rev(levels(to_plot$Term)),
                   labels = rev(c("Total Papers", "Cases",
                                  "Deaths", "Time",
                                  "Order in Effect", 
                                  "Time : Order in Effect"
                   ))) +
  scale_shape_manual(values = c(1, 19)) +
  guides(shape = FALSE) +
  ylab("Term") +
  ggtitle("Retail and Recreation") +
  fischeR::theme_saf_no_font() +
  theme(axis.text = element_text(size = 12),
        title = element_text(size = 14))

# combine panels for figure 1
(p1 + theme(axis.text.y = element_text(size = 10))) + 
  (p2 + theme(axis.text.y = element_blank(), 
              axis.title = element_blank()))

# residential model with demographic covariates
m1_residential <- 
  felm(residential_std ~ 
         paper_count_std + 
         cases_std_all + 
         deaths_std_all + 
         day_int_std * after_order +
         total_population_std + # county pop.
         POP_SQMI_std + # county pop. density
         median_age_std + # county median age
         pct_non_white_std + # share of pop not white
         pct_college_deg_std + # share of pop with any college deg
         pct_below_pov_lvl_std +  # share of pop below pov line
         pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
         pct_with_comp_broadband_std + # share with computer and broadband int
         pct_speak_only_english_std + # share that only speak english at home
         PCT_LACCESS_POP15_std + # share with low access to food
         state_capital + # state capital?
         dem_pct_std # partisan lean
       | 0 | 0 | state_abrv + fips, 
       data = google_mobility_2)
summary(m1_residential)
confint(m1_residential)

# recreation model with demographic covariates
m1_recreation <-
  felm(recreation_std ~
         paper_count_std +
         cases_std_all + 
         deaths_std_all + 
         day_int_std * after_order +
         total_population_std + # county pop.
         POP_SQMI_std + # county pop. density
         median_age_std + # county median age
         pct_non_white_std + # share of pop not white
         pct_college_deg_std + # share of pop with any college deg
         pct_below_pov_lvl_std +  # share of pop below pov line
         pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
         pct_with_comp_broadband_std + # share with computer and broadband int
         pct_speak_only_english_std + # share that only speak english at home
         PCT_LACCESS_POP15_std + # share with low access to food
         state_capital + # state capital?
         dem_pct_std # partisan lean
       | 0 | 0 | state_abrv + fips, 
       data = google_mobility_2)
summary(m1_recreation)
confint(m1_recreation)

# extract model info
to_plot_3 <- as.data.frame(summary(m1_residential)$coefficients)
to_plot_3$Term <- rownames(to_plot_3)
to_plot_3 <-
  to_plot_3 %>%
  filter(Term != "(Intercept)")
to_plot_3$Term <- factor(to_plot_3$Term, levels = to_plot_3$Term)
to_plot_3$sig <- ifelse(to_plot_3$`Pr(>|t|)` < 0.05, "Yes", "No")

# figure 2 left panel
p3 <- 
  ggplot(to_plot_3, aes(x = Estimate, y = Term, shape = sig)) +
  geom_point(size = 2.25) +
  geom_errorbarh(aes(xmin = (Estimate - 1.96 * `Cluster s.e.`), 
                     xmax = (Estimate + 1.96 * `Cluster s.e.`)),
                 height = 0.5) +
  geom_vline(xintercept = 0, lty = 2) +
  scale_y_discrete(limits = rev(levels(to_plot_3$Term)),
                   labels = rev(c("Total Papers", "Cases",
                                  "Deaths", "Time",
                                  "Order in Effect", "Population",
                                  "Population Density", "Median Age",
                                  "Pct. Not White", "Pct. w/ College Deg.",
                                  "Pct. Below Poverty Line",
                                  "Pct. at least 2x Above Poverty Line",
                                  "Pct. w/ Computer and Broadband",
                                  "Pct. Speaking only English at Home",
                                  "Pct. w/ Low Access to Food",
                                  "State Capital",
                                  "2016 Clinton Vote Share",
                                  "Time : Order in Effect"
                   ))) +
  scale_shape_manual(values = c(1, 19)) +
  guides(shape = FALSE) +
  ylab("Term") +
  ggtitle("Residential") +
  fischeR::theme_saf_no_font() +
  theme(axis.text = element_text(size = 12),
        title = element_text(size = 14))

# extract model information
to_plot_4 <- as.data.frame(summary(m1_recreation)$coefficients)
to_plot_4$Term <- rownames(to_plot_4)
to_plot_4 <-
  to_plot_4 %>%
  filter(Term != "(Intercept)")
to_plot_4$Term <- factor(to_plot_4$Term, levels = to_plot_4$Term)
to_plot_4$sig <- ifelse(to_plot_4$`Pr(>|t|)` < 0.05, "Yes", "No")

# figure 2 right panel
p4 <-
  ggplot(to_plot_4, aes(x = Estimate, y = Term, shape = sig)) +
  geom_point(size = 2.25) +
  geom_errorbarh(aes(xmin = (Estimate - 1.96 * `Cluster s.e.`), 
                     xmax = (Estimate + 1.96 * `Cluster s.e.`)),
                 height = 0.5) +
  geom_vline(xintercept = 0, lty = 2) +
  scale_y_discrete(limits = rev(levels(to_plot_3$Term)),
                   labels = rev(c("Total Papers", "Cases",
                                  "Deaths", "Time",
                                  "Order in Effect", "Population",
                                  "Population Density", "Median Age",
                                  "Pct. Not White", "Pct. w/ College Deg.",
                                  "Pct. Below Poverty Line",
                                  "Pct. at least 2x Above Poverty Line",
                                  "Pct. w/ Computer and Broadband",
                                  "Pct. Speaking only English at Home",
                                  "Pct. w/ Low Access to Food",
                                  "State Capital",
                                  "2016 Clinton Vote Share",
                                  "Time : Order in Effect"
                   ))) +
  scale_shape_manual(values = c(1, 19)) +
  guides(shape = FALSE) +
  ylab("Term") +
  ggtitle("Retail and Recreation") +
  fischeR::theme_saf_no_font() +
  theme(axis.text = element_text(size = 12),
        title = element_text(size = 14))

# combine panels of figure 2
(p3 + theme(axis.text.y = element_text(size = 10))) + 
  (p4 + theme(axis.text.y = element_blank(), 
              axis.title = element_blank()))

# remove any observations with missing data so that length(weights) = nrow(df)
mobility_reports_2 <-
  google_mobility_2 %>%
  dplyr::filter(!is.na(cases_std_all),
         !is.na(deaths_std_all),
         !is.na(order_date_std),
         !is.na(after_order),
         !is.na(total_population_std),
         !is.na(POP_SQMI_std),
         !is.na(median_age_std),
         !is.na(pct_non_white_std),
         !is.na(pct_college_deg_std),
         !is.na(pct_below_pov_lvl_std),
         !is.na(pct_above_2_pov_lvl_std),
         !is.na(pct_with_comp_broadband_std),
         !is.na(pct_speak_only_english_std),
         !is.na(state_capital),
         !is.na(dem_pct_std),
         !is.na(PCT_LACCESS_POP15_std),
         !is.na(paper_count_std))

# create df for counties with treatment and potential confounders
counties <- 
  google_mobility_2 %>%
  distinct(fips, total_population_std, POP_SQMI_std, median_age_std,
           pct_non_white_std, pct_college_deg_std, pct_below_pov_lvl_std,
           pct_above_2_pov_lvl_std, pct_with_comp_broadband_std, 
           pct_speak_only_english_std, PCT_LACCESS_POP15_std,
           state_capital, dem_pct_std, paper_count_std) %>%
  tidyr::drop_na()

# non-parametric approach to covariate balancing propensity score weights
set.seed(101)
start_time <- Sys.time()
cbgps_fit <- npCBPS(
  paper_count_std ~
    total_population_std + # county pop.
    POP_SQMI_std + # county pop. density
    median_age_std + # county median age
    pct_non_white_std + # share of pop not white
    pct_college_deg_std + # share of pop with any college deg
    pct_below_pov_lvl_std +  # share of pop below pov line
    pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
    pct_with_comp_broadband_std + # share with computer and broadband int
    pct_speak_only_english_std + # share that only speak english at home
    PCT_LACCESS_POP15_std + # share with low access to food
    state_capital + # state capital?
    dem_pct_std, # partisan lean
  data = counties, corprior = 0.0001
)
end_time <- Sys.time()

# mean correlation for balanced covariates
mean(abs(balance(cbgps_fit)$balanced))
mean(abs(balance(cbgps_fit)$unweighted))

# store balanced/unweighted correlations in df
balance_df <- as.data.frame(
  rbind(balance(cbgps_fit)$balanced, balance(cbgps_fit)$unweighted)
)
balance_df$group <- c(rep("Balanced", nrow(balance_df) / 2), 
                      rep("Unweighted", nrow(balance_df) / 2))
balance_df$group <- factor(balance_df$group, 
                           levels = c("Unweighted", "Balanced"))

# test to see if balancing reduces correlations with treatment
wilcox.test(balance_df$`Pearson Correlation`[which(balance_df$group == 
                                                     "Balanced")], 
            balance_df$`Pearson Correlation`[which(balance_df$group ==
                                                     "Unweighted")], 
            alternative = "less")

# plot figure 3A
ggplot(balance_df, aes(x = group, y = `Pearson Correlation`)) +
  geom_boxplot() +
  scale_x_discrete("Group") +
  ggtitle("A.") +
  fischeR::theme_saf_no_font() -> balance_cor_comp

# plot figure 3B
ggplot() +
  geom_vline(xintercept = 0, lty = 2) +
  geom_segment(aes(x = abs(balance(cbgps_fit)$unweighted),
                   xend = abs(balance(cbgps_fit)$balanced),
                   y = rownames(balance(cbgps_fit)$balanced),
                   yend = rownames(balance(cbgps_fit)$balanced)),
               arrow = arrow(length = unit(0.1, "cm"))) +
  scale_y_discrete(labels = rev(c("Population",
                              "State Capital",
                              "Population Density",
                              "Pct. w/ Computer and Broadband",
                              "Pct. Speaking only English at Home",
                              "Pct. Not White",
                              "Pct. w/ Low Access to Food",
                              "Pct. w/ College Deg.",
                              "Pct. Below Poverty Line",
                              "Pct. at least 2x Above Poverty Line",
                              "Median Age",
                              "2016 Clinton Vote Share"))) +
  ylab("Variable") +
  xlab("| Correlation |") +
  ggtitle("B.") +
  fischeR::theme_saf_no_font() -> change_cor_plot

# plot figure 3
balance_cor_comp + change_cor_plot

# store weights in df
counties$weights <- cbgps_fit$weights

# merge weights into daily data
mobility_reports_2 <-
  mobility_reports_2 %>%
  inner_join(counties)

# residential model with propensity score weighting
m1_residential_CBGPS <- 
  felm(residential_std ~ 
         paper_count_std + 
         cases_std_all + 
         deaths_std_all + 
         day_int_std * after_order +
         total_population_std + + # county pop.
         POP_SQMI_std + # county pop. density
         median_age_std + # county median age
         pct_non_white_std + # share of pop not white
         pct_college_deg_std + # share of pop with any college deg
         pct_below_pov_lvl_std +  # share of pop below pov line
         pct_above_2_pov_lvl_std + # share of pop at least 2x pov line
         pct_with_comp_broadband_std + # share with computer and broadband int
         pct_speak_only_english_std + # share that only speak english at home
         PCT_LACCESS_POP15_std + # share with low access to food
         state_capital + # state capital?
         dem_pct_std # partisan lean
       | 0 | 0 | state_abrv + fips, 
       data = mobility_reports_2, weights = mobility_reports_2$weights)
summary(m1_residential_CBGPS)
confint(m1_residential_CBGPS)

# recreation model with propensity score weighting
m1_recreation_CBGPS <- 
  felm(recreation_std ~ 
         paper_count_std + 
         cases_std_all +
         deaths_std_all +
         day_int_std * after_order +
         total_population_std + + # county pop.
         POP_SQMI_std + + # county pop. density
         median_age_std + + # county median age
         pct_non_white_std + + # share of pop not white
         pct_college_deg_std + + # share of pop with any college deg
         pct_below_pov_lvl_std + +  # share of pop below pov line
         pct_above_2_pov_lvl_std + + # share of pop at least 2x pov line
         pct_with_comp_broadband_std + + # share with computer and broadband int
         pct_speak_only_english_std + + # share that only speak english at home
         PCT_LACCESS_POP15_std + + # share with low access to food
         state_capital + # state capital?
         dem_pct_std # partisan lean
       | 0 | 0 | state_abrv + fips, 
       data = mobility_reports_2, weights = mobility_reports_2$weights)
summary(m1_recreation_CBGPS)
confint(m1_recreation_CBGPS)

# extract data for plotting
to_plot_5 <- as.data.frame(summary(m1_residential_CBGPS)$coefficients)
to_plot_5$Term <- rownames(to_plot_5)
to_plot_5 <-
  to_plot_5 %>%
  filter(Term != "(Intercept)")
to_plot_5$Term <- factor(to_plot_5$Term, levels = to_plot_5$Term)
to_plot_5$sig <- ifelse(to_plot_5$`Pr(>|t|)` < 0.05, "Yes", "No")

# produce figure 4 left panel
p5 <- 
  ggplot(to_plot_5, aes(x = Estimate, y = Term, shape = sig)) +
  geom_point(size = 2.25) +
  geom_errorbarh(aes(xmin = (Estimate - 1.96 * `Cluster s.e.`), 
                     xmax = (Estimate + 1.96 * `Cluster s.e.`)),
                 height = 0.5) +
  geom_vline(xintercept = 0, lty = 2) +
  scale_y_discrete(limits = rev(levels(to_plot_3$Term)),
                   labels = rev(c("Total Papers", "Cases",
                                  "Deaths", "Time",
                                  "Order in Effect", "Population",
                                  "Population Density", "Median Age",
                                  "Pct. Not White", "Pct. w/ College Deg.",
                                  "Pct. Below Poverty Line",
                                  "Pct. at least 2x Above Poverty Line",
                                  "Pct. w/ Computer and Broadband",
                                  "Pct. Speaking only English at Home",
                                  "Pct. w/ Low Access to Food",
                                  "State Capital",
                                  "2016 Clinton Vote Share",
                                  "Time : Order in Effect"
                   ))) +
  scale_shape_manual(values = c(1, 19)) +
  guides(shape = FALSE) +
  ylab("Term") +
  ggtitle("Residential") +
  fischeR::theme_saf_no_font() +
  theme(axis.text = element_text(size = 12),
        title = element_text(size = 14))

# extract model data for figure 4 right panel
to_plot_6 <- as.data.frame(summary(m1_recreation_CBGPS)$coefficients)
to_plot_6$Term <- rownames(to_plot_6)
to_plot_6 <-
  to_plot_6 %>%
  filter(Term != "(Intercept)")
to_plot_6$Term <- factor(to_plot_6$Term, levels = to_plot_6$Term)
to_plot_6$sig <- ifelse(to_plot_6$`Pr(>|t|)` < 0.05, "Yes", "No")

# produce figure 4 right panel
p6 <-
  ggplot(to_plot_6, aes(x = Estimate, y = Term, shape = sig)) +
  geom_point(size = 2.25) +
  geom_errorbarh(aes(xmin = (Estimate - 1.96 * `Cluster s.e.`), 
                     xmax = (Estimate + 1.96 * `Cluster s.e.`)),
                 height = 0.5) +
  geom_vline(xintercept = 0, lty = 2) +
  scale_y_discrete(limits = rev(levels(to_plot_3$Term)),
                   labels = rev(c("Total Papers", "Cases",
                                  "Deaths", "Time",
                                  "Order in Effect", "Population",
                                  "Population Density", "Median Age",
                                  "Pct. Not White", "Pct. w/ College Deg.",
                                  "Pct. Below Poverty Line",
                                  "Pct. at least 2x Above Poverty Line",
                                  "Pct. w/ Computer and Broadband",
                                  "Pct. Speaking only English at Home",
                                  "Pct. w/ Low Access to Food",
                                  "State Capital",
                                  "2016 Clinton Vote Share",
                                  "Time : Order in Effect"
                   ))) +
  scale_shape_manual(values = c(1, 19)) +
  guides(shape = FALSE) +
  ylab("Term") +
  ggtitle("Retail and Recreation") +
  fischeR::theme_saf_no_font() +
  theme(axis.text = element_text(size = 12),
        title = element_text(size = 14))

# combine panels for figure 4
(p5 + theme(axis.text.y = element_text(size = 10))) + 
  (p6 + theme(axis.text.y = element_blank(), 
              axis.title = element_blank()))

# Produce publication ready tables
tab_out <- summary(m_residential)
tab_out <- as.data.frame(tab_out$coefficients[, c(1, 2, 4)])
colnames(tab_out) <- c("b", "s.e.", "p")
tab_out_confint <- confint(m_residential)
tab_out_confint <- paste("$", signif( tab_out_confint[, 1], 2), "$, $", 
                         signif(tab_out_confint[, 2], 2), "$",
                         sep = "")
tab_out$`95% CI` <- tab_out_confint
rownames(tab_out) <- c("Intercept", "Total Papers")
tab_out$Var <- rownames(tab_out)
tab_out$b <- signif(tab_out$b, 2)
tab_out$`s.e.` <- signif(tab_out$`s.e.`, 2)
tab_out$p <- signif(tab_out$p, 2)

tab_out2 <- summary(m0_residential)
tab_out2 <- as.data.frame(tab_out2$coefficients[, c(1, 2, 4)])
colnames(tab_out2) <- c("b", "s.e.", "p")
tab_out2_confint <- confint(m0_residential)
tab_out2_confint <- paste("$", signif( tab_out2_confint[, 1], 2), "$, $", 
                          signif(tab_out2_confint[, 2], 2), "$",
                          sep = "")
tab_out2$`95% CI` <- tab_out2_confint
rownames(tab_out2) <- c("Intercept", "Total Papers", 
                        "Cases", "Deaths", "Time",
                        "Order in Effect (binary)",
                        "Time : Order in Effect")
tab_out2$Var <- rownames(tab_out2)
tab_out2$b <- signif(tab_out2$b, 2)
tab_out2$`s.e.` <- signif(tab_out2$`s.e.`, 2)
tab_out2$p <- signif(tab_out2$p, 2)

tab_out3 <- summary(m1_residential)
tab_out3 <- as.data.frame(tab_out3$coefficients[, c(1, 2, 4)])
colnames(tab_out3) <- c("b", "s.e.", "p")
tab_out3_confint <- confint(m1_residential)
tab_out3_confint <- paste("$", signif(tab_out3_confint[, 1], 2), "$, $", 
                          signif(tab_out3_confint[, 2], 2), "$",
                          sep = "")
tab_out3$`95% CI` <- tab_out3_confint
rownames(tab_out3) <- c("Intercept", "Total Papers", 
                        "Cases", "Deaths", "Time",
                        "Order in Effect (binary)",
                        "Population Size",
                        "Population Density",
                        "Median Age",
                        "Pct. Not White",
                        "Pct. w/ College Degree",
                        "Pct. Below Poverty Level",
                        "Pct. at least 2x Above Poverty Level",
                        "Pct. w/ Computer and Broadband",
                        "Pct. Speak only English at Home",
                        "Pct. w/ Low Access to Food",
                        "State Capital",
                        "2016 Clinton Vote Share",
                        "Time : Order in Effect")
tab_out3$Var <- rownames(tab_out3)
tab_out3$b <- signif(tab_out3$b, 2)
tab_out3$`s.e.` <- signif(tab_out3$`s.e.`, 2)
tab_out3$p <- signif(tab_out3$p, 2)

tab_out4 <- summary(m1_residential_CBGPS)
tab_out4 <- as.data.frame(tab_out4$coefficients[, c(1, 2, 4)])
colnames(tab_out4) <- c("b", "s.e.", "p")
tab_out4_confint <- confint(m1_residential_CBGPS)
tab_out4_confint <- paste("$", signif(tab_out4_confint[, 1], 2), "$, $", 
                          signif(tab_out4_confint[, 2], 2), "$",
                          sep = "")
tab_out4$`95% CI` <- tab_out4_confint
rownames(tab_out4) <- c("Intercept", "Total Papers", 
                        "Cases", "Deaths", "Time",
                        "Order in Effect (binary)",
                        "Population Size",
                        "Population Density",
                        "Median Age",
                        "Pct. Not White",
                        "Pct. w/ College Degree",
                        "Pct. Below Poverty Level",
                        "Pct. at least 2x Above Poverty Level",
                        "Pct. w/ Computer and Broadband",
                        "Pct. Speak only English at Home",
                        "Pct. w/ Low Access to Food",
                        "State Capital",
                        "2016 Clinton Vote Share",
                        "Time : Order in Effect")
tab_out4$Var <- rownames(tab_out4)
tab_out4$b <- signif(tab_out4$b, 2)
tab_out4$`s.e.` <- signif(tab_out4$`s.e.`, 2)
tab_out4$p <- signif(tab_out4$p, 2)

tab_out5 <-
  tab_out %>%
  full_join(tab_out2, by = c("Var" = "Var")) %>%
  full_join(tab_out3, by = c("Var" = "Var")) %>%
  full_join(tab_out4, by = c("Var" = "Var"))
rownames(tab_out5) <- tab_out5$Var
tab_out5 <- tab_out5[, -5]

tab_out6 <- summary(m_recreation)
tab_out6 <- as.data.frame(tab_out6$coefficients[, c(1, 2, 4)])
colnames(tab_out6) <- c("b", "s.e.", "p")
tab_out6_confint <- confint(m_recreation)
tab_out6_confint <- paste("$", signif(tab_out6_confint[, 1], 2), "$, $", 
                          signif(tab_out6_confint[, 2], 2), "$",
                          sep = "")
tab_out6$`95% CI` <- tab_out6_confint
rownames(tab_out6) <- c("Intercept", "Total Papers")
tab_out6$Var <- rownames(tab_out6)
tab_out6$b <- signif(tab_out6$b, 2)
tab_out6$`s.e.` <- signif(tab_out6$`s.e.`, 2)
tab_out6$p <- signif(tab_out6$p, 2)

tab_out7 <- summary(m0_recreation)
tab_out7 <- as.data.frame(tab_out7$coefficients[, c(1, 2, 4)])
colnames(tab_out7) <- c("b", "s.e.", "p")
tab_out7_confint <- confint(m0_recreation)
tab_out7_confint <- paste("$", signif(tab_out7_confint[, 1], 2), "$, $", 
                          signif(tab_out7_confint[, 2], 2), "$",
                          sep = "")
tab_out7$`95% CI` <- tab_out7_confint
rownames(tab_out7) <- c("Intercept", "Total Papers", 
                        "Cases", "Deaths", "Time",
                        "Order in Effect (binary)",
                        "Time : Order in Effect")
tab_out7$Var <- rownames(tab_out7)
tab_out7$b <- signif(tab_out7$b, 2)
tab_out7$`s.e.` <- signif(tab_out7$`s.e.`, 2)
tab_out7$p <- signif(tab_out7$p, 2)

tab_out8 <- summary(m1_recreation)
tab_out8 <- as.data.frame(tab_out8$coefficients[, c(1, 2, 4)])
colnames(tab_out8) <- c("b", "s.e.", "p")
tab_out8_confint <- confint(m1_recreation)
tab_out8_confint <- paste("$", signif(tab_out8_confint[, 1], 2), "$, $", 
                          signif(tab_out8_confint[, 2], 2), "$",
                          sep = "")
tab_out8$`95% CI` <- tab_out8_confint
rownames(tab_out8) <- c("Intercept", "Total Papers", 
                        "Cases", "Deaths", "Time",
                        "Order in Effect (binary)",
                        "Population Size",
                        "Population Density",
                        "Median Age",
                        "Pct. Not White",
                        "Pct. w/ College Degree",
                        "Pct. Below Poverty Level",
                        "Pct. at least 2x Above Poverty Level",
                        "Pct. w/ Computer and Broadband",
                        "Pct. Speak only English at Home",
                        "Pct. w/ Low Access to Food",
                        "State Capital",
                        "2016 Clinton Vote Share",
                        "Time : Order in Effect")
tab_out8$Var <- rownames(tab_out8)
tab_out8$b <- signif(tab_out8$b, 2)
tab_out8$`s.e.` <- signif(tab_out8$`s.e.`, 2)
tab_out8$p <- signif(tab_out8$p, 2)

tab_out9 <- summary(m1_recreation_CBGPS)
tab_out9 <- as.data.frame(tab_out9$coefficients[, c(1, 2, 4)])
colnames(tab_out9) <- c("b", "s.e.", "p")
tab_out9_confint <- confint(m1_recreation_CBGPS)
tab_out9_confint <- paste("$", signif(tab_out9_confint[, 1], 2), "$, $", 
                          signif(tab_out9_confint[, 2], 2), "$",
                          sep = "")
tab_out9$`95% CI` <- tab_out9_confint
rownames(tab_out9) <- c("Intercept", "Total Papers", 
                        "Cases", "Deaths", "Time",
                        "Order in Effect (binary)",
                        "Population Size",
                        "Population Density",
                        "Median Age",
                        "Pct. Not White",
                        "Pct. w/ College Degree",
                        "Pct. Below Poverty Level",
                        "Pct. at least 2x Above Poverty Level",
                        "Pct. w/ Computer and Broadband",
                        "Pct. Speak only English at Home",
                        "Pct. w/ Low Access to Food",
                        "State Capital",
                        "2016 Clinton Vote Share",
                        "Time : Order in Effect")
tab_out9$Var <- rownames(tab_out9)
tab_out9$b <- signif(tab_out9$b, 2)
tab_out9$`s.e.` <- signif(tab_out9$`s.e.`, 2)
tab_out9$p <- signif(tab_out9$p, 2)

tab_out10 <-
  tab_out6 %>%
  full_join(tab_out7, by = c("Var" = "Var")) %>%
  full_join(tab_out8, by = c("Var" = "Var")) %>%
  full_join(tab_out9, by = c("Var" = "Var"))
rownames(tab_out10) <- tab_out10$Var
tab_out10 <- tab_out10[, -5]

# stay at home table
stargazer::stargazer(tab_out5, summary = FALSE, digits = NA)

# retail and recreation table
stargazer::stargazer(tab_out10, summary = FALSE, digits = NA)
